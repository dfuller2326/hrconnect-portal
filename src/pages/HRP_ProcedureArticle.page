<apex:page showHeader="false" sidebar="false" standardStylesheets="false" docType="html-5.0" standardController="Procedure__kav" extensions="HRP_ProcedureArticleController" language="{$User.Language}">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title id="titleContainer"></title>
        <!-- css/js -->
        <apex:stylesheet value="{!URLFOR($Resource.HRPResources, '/css/salesforce1style.css')}"/>
        <apex:stylesheet value="{!URLFOR($Resource.HRPResources, '/css/bootstrap.min.css')}"/>
        <apex:stylesheet value="{!URLFOR($Resource.HRPResources, '/css/icons_50X50.css')}"/>
        <apex:stylesheet value="{!$Resource.HRP_MainCSS}"/>
        <apex:includeScript value="{!URLFOR($Resource.HRPResources, '/js/jquery.min.js')}"/>
        <apex:includeScript value="{!$Resource.HRP_CommonUtils}"/>

        <!-- jQuery No Conflict Variable -->
        <script type="text/javascript">
            var $j = jQuery.noConflict();
        </script>

    </head>
    <body>
        <!-- header -->
        <c:HRP_Header showSearch="true"/>
        <div class="container topSectionContainer" id="articleDetailContainer">
            <!-- error panel -->
            <div class="alert alert-danger hidden" role="alert" id="errorPanel">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="h4" id="errorHeading">{!$Label.Error_Panel_Head}</span>
                <p></p>
            </div>

            <div id="breadcrumbsContainer"></div>

            <!-- detail section -->
            <div class="outer-panel spinner ringed">
                <div class="row" id="detailSection" style="display:none">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 prm" id="articleDetailLeft">
                        <div class="panel panel-default">
                            <div class="panel-heading" id="articleDetailLeftHeader">
                               <div id="articleHeaderContainer"></div>
                            </div>
                            <div class="panel-body" id="articleBodyContainer"></div>
                            <div class="panel-footer">
                                <apex:form >
                                    <div class="pull-right">
                                        <table>
                                            <tr>
                                                <td class="phs ptxs f3">{!$Label.HRP_Rate_This_Article}</td>
                                                <td class="phs">
                                                    <apex:vote objectId="{!$CurrentPage.parameters.id}" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </apex:form>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 plm" id="articleDetailRight">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div id="actionsContainer"></div>

                                <div id="contactContainer"></div>

                                <!-- Related Services -->
                                <c:HRP_RelatedServices id="relatedServicesCmp"/>

                                <!-- Important Documents -->
                                <c:HRP_Documents id="relatedDocsCmp"/>

                                <!-- Related Article -->
                                <c:HRP_RelatedArticles id="relatedArticlesCmp"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <!-- Footer -->
            <c:HRP_Footer id="detailFooter"/>
        </div>
    </body>

    <!-- Javascript -->
    <apex:includeScript value="{!URLFOR($Resource.HRPResources, '/js/jquery.mobile.custom.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.HRPResources, '/js/bootstrap.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.HRPResources, '/js/json2.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.HRPResources, '/js/underscore.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.HRPResources, '/js/backbone.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.HRPResources, '/js/mediator.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.HRPResources, '/js/bootstrap-datepicker.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.HRPResources, '/js/typeahead.bundle.min.js')}"/>
    <apex:includeScript value="{!$Resource.HRP_ArticleDetailJS}"/>

    <script type="text/javascript">

        // Checking for 'Mobile' device string in user agent per MDN best practice
        var isMobile = navigator.userAgent.match(/Mobi/gi) != null;
        var isLandscape = false;

        // set up the mediator Object for search bar in the header
        var mediatorObj = new Mediator();

        // initialize Backone setup
        var remotingErrorMsg = '{!$Label.Remoting_Error_Message}';
        var ArticleDetail = Biogen_ArticleDetail($j, '{!$RemoteAction.HRP_ProcedureArticleController.getPageData}', remotingErrorMsg);

        // Get Knowledge Article Id from URL Parameter
        ArticleDetail.kaId = "{!$CurrentPage.parameters.id}";

        // initialize Backbone resources
        var ArticleDetail_Collection = ArticleDetail.getCollection();
        var ArticleHeader_View = ArticleDetail.getArticleHeaderView();
        var ArticleBody_View = ArticleDetail.getArticleBodyView();
        var Title_View = ArticleDetail.getTitleView();
        var Breadcrumbs_View = ArticleDetail.getBreadcrumbsView();
        var ArticleActions_View = ArticleDetail.getActionsView();
        var ArticleContact_View = ArticleDetail.getContactView();

        // init collection to fetch the data from backend
        var articleDetailCollection = new ArticleDetail_Collection();

        $j(document).ready(function() {

            // Initialize Views
            var headerView = new ArticleHeader_View({
                el: '#articleHeaderContainer',
                collection: articleDetailCollection
            });

            var bodyView = new ArticleBody_View({
                el: '#articleBodyContainer',
                collection: articleDetailCollection
            });

             var titleView = new Title_View({
                el: '#titleContainer',
                collection: articleDetailCollection
            });

            var breadcrumbsView = new Breadcrumbs_View({
                el: '#breadcrumbsContainer',
                collection: articleDetailCollection
            });

            var actionsView = new ArticleActions_View({
                el: '#actionsContainer',
                collection: articleDetailCollection
            });

            var contactView = new ArticleContact_View({
                el: '#contactContainer',
                collection: articleDetailCollection
            });

            // subscribe to search mediatorObj event from search bar component - search value must be 2 characters or greater or an error will be thrown
            mediatorObj.subscribe("Search", function(data) {
                if(data.params && data.params.inputVal) {
                    if(data.params.inputVal.length < 2) {
                        $j("html, body").animate({ scrollTop: 0 }, "slow");
                        // show error
                        $j('#errorPanel').find('p').html('{!$Label.Search_Error}');
                        $j('#errorPanel').removeClass('hidden').fadeIn();
                    } else {
                        location.href = '/apex/HRP_SearchResults?q=' + data.params.inputVal;
                    }
                }
            });

            console.log($j('{!$Component.detailFooter.footerContainer}').css("display"));
        });
    </script>

    <script type="text/javascript">
        function checkEmpty(el) {
            var html = $j.trim(el);
            var isEmpty = false;
            if(html.length === 0) {
                isEmpty = true;
            }
            return isEmpty;
        }
    </script>

    <script type="text/template" id="articleHeaderTemplate">
        <% if(!article) { %>
            <% return; %>
        <% } else { %>
            <% recordRecentArticle(window.location.href, article.Title) %>
        <% } %>
        <div class="row articleHeaderRow">
            <div class="col-sm-12 col-md-12 col-lg-7 articleHeaderTitleContainer">
                <h4 class="articleHeaderTitle"><%= article.Title %></h4>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-5 articleRatingContainer">
                <div class="dib starRatingContainer">
                    <div class="star-rating">
                        <% if(articleStats.VoteStats) { %>
                            <%_.each(articleStats.VoteStats.records, function(item, index){ %>
                                <% if(item.Channel === "App") { %>
                                    <% var voteScore = (item.NormalizedScore) * 20; %>
                                    <% console.log(voteScore); %>
                                    <div class="star-rating-t" style="width: <%= voteScore %>%;"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                                    <div class="star-rating-b">
                                        <span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span>
                                    </div>
                                <% } %>
                            <% }); %>
                        <% } %>
                    </div>
                </div>
                <% if(articleStats.Votes) { %>
                    <div class="border-right border--2 dib prm"><%= articleStats.Votes.records.length %>&nbsp;{!$Label.HRP_Reviews}</div>
                <% } else { %>
                    <div class="border-right border--2 dib prm">{!$Label.HRP_No_Reviews}</div>
                <% } %>
                <span class="glyphicon glyphicon-print" aria-hidden="true" role="button" id="articleDetailPrintIcon"/>
            </div>
        </div>
    </script>

    <!-- Templates -->

    <script type="text/template" id="articleBodyTemplate">
        <%= article.Body__c %>
    </script>

    <script type="text/template" id="titleTemplate">
        <%= article.Title %>
    </script>

    <script type="text/template" id="breadcrumbsTemplate">
        <!-- breadcrumbs -->
        <ol class="breadcrumb nav-path">
            <li><a href="/apex/HRP_Home">{!$Label.HRP_Home_Breadcrumb}</a></li>
            <li><a href="/apex/HRP_SearchResults">{!$Label.HRP_Search_Breadcrumb}</a></li>
            <li class="active"><a href="javascript:void(0);"><%= article.Title %></a></li>
        </ol>
    </script>

    <script type="text/template" id="contactTemplate">
        <!-- contact detail section -->
        <% if(article.Contact_Information__c && article.Contact_Information__c !== '') { %>
            <div class="panel panel-default rightSubPanel">
                <div class="panel-heading">
                  <span class="panel-title"><b>{!$Label.HRP_Contact_Information}</b></span>
                </div>
                <div class="panel-body">
                    <div style="margin-bottom:-20px"><%= article.Contact_Information__c %></div>
                </div>
            </div>
        <% } %>
    </script>

    <script type="text/template" id="actionsTemplate">
        <% var actionUrl = ''; %>
        <% if(article.Action_1_Label__c && article.Action_1_Label__c !== "") { %>
            <% actionUrl = ArticleDetail.getActionURL(article.Action_1_URL__c); %>
            <div class="text-center">
                <a href="<%= actionUrl %>" target="_blank"><button type="button" class="btn articleDtlActionButton" style="background-color:#<%= article.Action_1_Bg_Color__c %>"><%= article.Action_1_Label__c %></button></a>
            </div>
        <% } %>
        <% if(article.Action_2_Label__c && article.Action_2_Label__c !== "") { %>
            <% actionUrl = ArticleDetail.getActionURL(article.Action_2_URL__c); %>
            <div class="text-center">
                 <a href="<%= actionUrl %>" target="_blank"><button type="button" class="btn articleDtlActionButton" style="background-color:#<%= article.Action_2_Bg_Color__c %>"><%= article.Action_2_Label__c %></button></a>
            </div>
        <% } %>
        <% if(article.Action_3_Label__c && article.Action_3_Label__c !== "") { %>
            <% actionUrl = ArticleDetail.getActionURL(article.Action_3_URL__c); %>
            <div class="text-center">
                 <a href="<%= actionUrl %>" target="_blank"><button type="button" class="btn articleDtlActionButton" style="background-color:#<%= article.Action_3_Bg_Color__c %>"><%= article.Action_3_Label__c %></button></a>
            </div>
        <% } %>
    </script>

</apex:page>