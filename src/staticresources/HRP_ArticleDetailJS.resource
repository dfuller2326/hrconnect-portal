/* Article Detail Page Backbone Controller */
var Biogen_ArticleDetail = function($, searchResultsMethod, remotingErrorMsg, mediator) {
	// Backbone sync override - A sync event is fired once the "request or GET" event completes successfully with the server.
	function sync(method, model, options) {
		switch (method) {
			case 'read':
				return getPageData(method, model, options);
				break;
		}
	}

	// Custom read function
	function getPageData(method, model, options) {
		Visualforce.remoting.Manager.invokeAction(searchResultsMethod, ArticleDetail.kaId,
			function(result, event) {
				// remove spinner
				$('.outer-panel').removeClass('spinner ringed').css('min-height', '0');
				if (event.status) {
					var response = $.parseJSON(result);
					if (response.error == null) {
						options.success(response);
						// Check sidebar content
						if(response.article.Contact_Information__c === undefined && response.article.Action_1_Label__c === undefined && response.article.Action_2_Label__c === undefined && response.article.Action_3_Label__c === undefined && response.importantDocs.length === 0 && response.relatedArticles.length === 0 && response.relatedQA.length === 0) {
							$('#articleDetailRight').hide();
							$('#articleDetailLeft').removeClass('col-xs-12 col-sm-12 col-md-8 col-lg-8 prm')
												   .addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');
						}
						// show detail section after resetting the column width
						$('#detailSection').fadeIn();
					} else {
						ArticleDetail.displayError(response.error.errCode, response.error.errMsg);
					}
				} else {
					ArticleDetail.displayError(remotingErrorMsg);
				}
			}, { escape: false }
		);
	}

	// model
	var articleDetailModel = Backbone.Model.extend({
		id: 'article',
		idAttribute: 'Id'
	});

	// collection
	var articleDetailCollection = Backbone.Collection.extend({
		model: articleDetailModel,
		sync: sync,
		initialize: function() {
			this.fetch();
		}
	});

	// Article Header View
	var articleHeaderView = Backbone.View.extend({
		initialize: function() {
			// Self variable for holding the context of the parent view
			var self = this;
			self.collection.on('add reset re-render', self.render, self);
			self.template = _.template($("#articleHeaderTemplate").html());
		},
		events: {
			"click #articleDetailPrintIcon" : "printArticle"
		},
		printArticle: function() {
			window.print();
		},
		render: function() {
			var model = this.collection.at(0);
			if (model && model.attributes.article) {
				this.$el.html(this.template(model.attributes));
			}
			return this;
		}
	});

	// Article Body View
	var articleBodyView = Backbone.View.extend({
		initialize: function() {
			// Self variable for holding the context of the parent view
			var self = this;
			self.collection.on('add reset re-render', self.render, self);
			self.template = _.template($("#articleBodyTemplate").html());
		},
		render: function() {
			var model = this.collection.at(0);
			if (model && model.attributes.article) {
				this.$el.html(this.template(model.attributes));
			}
			return this;
		}
	});

	// Page Title Tab View
	var titleView = Backbone.View.extend({
		initialize: function() {
			// Self variable for holding the context of the parent view
			var self = this;
			self.collection.on('add reset re-render', self.render, self);
			self.template = _.template($("#titleTemplate").html());
		},
		render: function() {
			var model = this.collection.at(0);
			if (model && model.attributes.article) {
				this.$el.html(this.template(model.attributes));
			}
			return this;
		}
	});

	// Breadcrumbs View
	var breadcrumbsView = Backbone.View.extend({
		initialize: function() {
			// Self variable for holding the context of the parent view
			var self = this;
			self.collection.on('add reset re-render', self.render, self);
			self.template = _.template($("#breadcrumbsTemplate").html());
		},
		render: function() {
			var model = this.collection.at(0);
			if (model && model.attributes.article) {
				this.$el.html(this.template(model.attributes));
			}
			return this;
		}
	});

	// ArticleActions View
	var actionsView = Backbone.View.extend({
		initialize: function() {
			// Self variable for holding the context of the parent view
			var self = this;
			self.collection.on('add reset re-render', self.render, self);
			self.template = _.template($("#actionsTemplate").html());
		},
		render: function() {
			var model = this.collection.at(0);
			if (model && model.attributes.article) {
				this.$el.html(this.template(model.attributes));
			}
			var elHtml = $.trim(this.$el.html());
			if(elHtml.length === 0) {
				ArticleDetail.actionsEmpty = true;
			}
			this.checkEmpty();
			return this;
		},
		checkEmpty: function() {
			var elHtml = $.trim(this.$el.html());
			console.log(elHtml);
			var isEmpty = false;
			if(elHtml.length === 0) {
				isEmpty = true;
			}
			return isEmpty;
		}
	});

	// Contact View
	var contactView = Backbone.View.extend({
		initialize: function() {
			// Self variable for holding the context of the parent view
			var self = this;
			self.collection.on('add reset re-render', self.render, self);
			self.template = _.template($("#contactTemplate").html());
		},
		render: function() {
			var model = this.collection.at(0);
			if (model && model.attributes.article) {
				this.$el.html(this.template(model.attributes));
			}
			this.checkEmpty();
			return this;
		},
		checkEmpty: function() {
			var elHtml = $.trim(this.$el.html());
			var isEmpty = false;
			if(elHtml.length === 0) {
				isEmpty = true;
			}
			return isEmpty;
		}
	});

	return {
		getCollection: function() {
			return articleDetailCollection;
		},
		getArticleHeaderView: function() {
			return articleHeaderView;
		},
		getArticleBodyView: function() {
			return articleBodyView;
		},
		getTitleView: function() {
			return titleView;
		},
		getBreadcrumbsView: function() {
			return breadcrumbsView;
		},
		getActionsView: function() {
			return actionsView;
		},
		getContactView: function() {
			return contactView;
		},
		getActionURL : function(url) {
			if(!url || url.trim() == '') {
				return 'javascript:void(0);';
			}

			// if url starts with '/apex' or 'http:// | https://'
			var internalPattern = /^((\/apex)\/)/;
			var httpPattern = /^((http|https):\/\/)/;
			if(internalPattern.test(url) || httpPattern.test(url)) {
				return url;
			}

			// if url starts with 'www.'
			var wwwPattern = /^((www)\.)/;
			var httpsPrefix = 'https://';
			if(wwwPattern.test(url)) {
		    	return httpsPrefix + url;
		    }

			var fullPrefix = 'https://www.';
	    	return fullPrefix + url;
		},
		kaId: '',
		actionsEmpty: false,
		contactEmpty:false,
		displayError: function(errorCode, errorMsg) {
			$('#detailSection').fadeIn();
			if(errorCode && errorCode !== '') {
		    	$('#errorHeading').html(errorCode);
			}
		    $('#errorPanel').find('p').html(errorMsg);
		    $('#errorPanel').removeClass('hidden').fadeIn();
		}
	}
}
