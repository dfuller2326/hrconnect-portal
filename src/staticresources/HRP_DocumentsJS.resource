// Backbone Controller for Important Documents Component
var Biogen_Documents = function($, searchResultsMethod, remotingErrorMsg) {
	// Backbone sync override - A sync event is fired once the "request or GET" event completes successfully with the server.
	function sync(method, model, options) {
		switch (method) {
			case 'read':
				return getPageData(method, model, options);
				break;
		}
	}

	// Custom read function
	function getPageData(method, model, options) {
		Visualforce.remoting.Manager.invokeAction(searchResultsMethod, Documents.kaId,
			function(result, event) {
				// remove spinner
				if (event.status) {
					var response = $.parseJSON(result);
					if (response.error == null) {
						options.success(response);
					} else {
						Documents.displayError(response.error.errCode, response.error.errMsg);
					}
				} else {
					Documents.displayError(remotingErrorMsg);
				}
			}, { escape: false }
		);
	}

	// model
	var documentsModel = Backbone.Model.extend({
		id: 'article',
		idAttribute: 'Id'
	});

	// collection
	var documentsCollection = Backbone.Collection.extend({
		model: documentsModel,
		sync: sync,
		initialize: function() {
			this.fetch();
		}
	});

	// Article Search Results View
	var documentsView = Backbone.View.extend({
		initialize: function() {
			// Self variable for holding the context of the parent view
			var self = this;
			self.collection.on('add reset re-render', self.render, self);
			self.template = _.template($("#documentsTemplate").html());
		},
		render: function() {
			var model = this.collection.at(0);
			if (model && model.attributes.importantDocs) {
				this.$el.html(this.template(model.attributes));
			}
			var elHtml = $.trim(this.$el.html());
			if(elHtml.length === 0) {
				Documents.documentsEmpty = true;
			}
			return this;
		}
	});

	return {
		getCollection: function() {
			return documentsCollection;
		},
		getDocumentsView: function() {
			return documentsView;
		},
		kaId: '',
		documentsEmpty: false,
		displayError: function(errorCode, errorMsg){
			if(errorCode && errorCode !== '') {
		    	$('#errorHeading').html(errorCode);
			}
		    $('#errorPanel').find('p').html(errorMsg);
		    $('#errorPanel').removeClass('hidden').fadeIn(); 
		}
	}
}
