@isTest(SeeAllData=false)
private class HRP_HomeControllerTest {

   private static List<HRP_Content__c> hrpContentList;

     private static void setupHRPContent(String recordType){
    hrpContentList = new List<HRP_Content__c>();
    Id recordTypeId = [SELECT Id From RecordType Where SObjectType = 'HRP_Content__c' AND DeveloperName = :recordType][0].Id;
    for(Integer i = 0; i < 5; i++){
      HRP_Content__c hrp = HRP_TestDataFactory.buildHRPContent(recordTypeId);
      hrpContentList.add(hrp);
    }
    insert hrpContentList;
  }

    private static testMethod void getHomePageData_Positive() {
        setupHRPContent(HRP_Constants.HR_CONTENT_TILE);
        setupHRPContent(HRP_Constants.HR_CONTENT_CAROUSEL);

        Test.startTest();
          String str = HRP_HomeController.getHomePageData();
           Map<String, Object> mapObj = (Map<String,Object>) JSON.deserializeUntyped(str);
           List<Object> carouselContent = (List<Object>) mapObj.get('carouselContent');
           List<Object> tileContent = (List<Object>) mapObj.get('tileContent');
        Test.stopTest();
         System.assertEquals(5, carouselContent.size());
         System.assertEquals(5, tileContent.size());

    }

    private static testMethod void getsetCurrentUser_Positive() {
        User u = [SELECT Id, Country, ProfileId, SmallPhotoUrl FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        Test.startTest();
          HRP_HomeController controller = new HRP_HomeController();
          controller.currentUser = u;
        Test.stopTest();
        System.assertEquals(u,controller.currentUser );
    }

    private static testMethod void getsetMobile_Positive() {
       Test.startTest();
          HRP_HomeController controller = new HRP_HomeController();
          controller.mobile = true;
        Test.stopTest();
        System.assertEquals(true,controller.mobile );
    }

    private static testMethod void setErrorInfo_Positive() {
       Test.startTest();
         HRP_HomeController.HomeResponse respObj = new HRP_HomeController.HomeResponse();
          respObj.error = new HRP_ErrorInfo(System.Label.Home_Page_Error_Heading, 'Exception Message');
        Test.stopTest();
        System.assertNotEquals(null,respObj.error );
    }
}