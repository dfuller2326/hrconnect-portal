@isTest(SeeAllData=false)
private class HRP_SearchResultsControllerTest {

  private static List<Procedure__kav> procArticles = new List<Procedure__kav>();
  private static List<Id> procArticleIds = new List<Id>();
  private static List<FAQ__kav> faqArticles = new List<FAQ__kav>();
  private static List<Id> faqArticleIds = new List<Id>();
  private static List<HRP_Settings__c> hrpSettings = new List<HRP_Settings__c>();

  private static void setupProcedureArticles(){
    for(Integer i = 0; i < 5; i++){
      Procedure__kav article = HRP_TestDataFactory.buildProcedureArticle();
      article.title += ' ' + i;
      article.UrlName += i;
      procArticles.add(article);
    }
    insert procArticles;

    procArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId FROM Procedure__kav WHERE Language = 'en_US'
      AND PublishStatus = 'Draft'];

    for(Procedure__kav article : procArticles){
      KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
    }

    for(Integer i = 0; i < 5; i++){
      procArticleIds.add(procArticles[i].Id);
    }
  }

  private static void setupFAQArticles(){
    for(Integer i = 0; i < 5; i++){
      FAQ__kav article = HRP_TestDataFactory.buildFAQArticle();
      article.title += ' ' + i;
      article.UrlName += i;
      faqArticles.add(article);
    }
    insert faqArticles;

    faqArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId FROM FAQ__kav WHERE Language = 'en_US'
      AND PublishStatus = 'Draft'];

    for(FAQ__kav article : faqArticles){
      KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
    }

    for(Integer i = 0; i < 5; i++){
      faqArticleIds.add(faqArticles[i].Id);
    }
  }

  private static void setupHRPSettings(){
    hrpSettings = HRP_TestDataFactory.buildHRPSettings();
    insert hrpSettings;
  }


  private static testMethod void getPageData_Positive() {
    setupHRPSettings();
    setupProcedureArticles();
    setupFAQArticles();

    Test.startTest();
      List<Id> fixedSearchResults= new List<Id>();
      fixedSearchResults.addAll(procArticleIds);
      fixedSearchResults.addAll(faqArticleIds);
      Test.setFixedSearchResults(fixedSearchResults);

      String str = HRP_SearchResultsController.getPageData('Procedure');
      Map<String, Object> mapObj = (Map<String,Object>) JSON.deserializeUntyped(str);
      List<Object> secondaryResults = (List<Object>) mapObj.get('secondaryResults');
      List<Object> primaryResults = (List<Object>) mapObj.get('primaryResults');
    Test.stopTest();

   	System.assertEquals(5, primaryResults.size());
  	System.assertEquals(5, secondaryResults.size());
  }

  private static testmethod void getCurrentUser_positive(){
    Test.startTest();
    	HRP_SearchResultsController controller = new HRP_SearchResultsController();
      User user = controller.currentUser;
    Test.stopTest();

    System.assertEquals('en_US',user.LanguageLocaleKey);
  }
}