public with sharing class HRP_HeaderController {
	
	public Boolean showWorkdayApex { 
        get{
             string userprofile = HRConnectCommon.userprofilename;
             if (userprofile.contains(BiogenConstants.PROSPECTIVE_EMPLOYEE_PROFILE) || userprofile.contains(BiogenConstants.FORMER_EMPLOYEE_PROFILE))
                return false;
            return true;
        } 
        set; 
    }
    
	public User currentUser {
		get {
			if(currentUser == null) {
				currentUser = [SELECT Id, LanguageLocaleKey, SmallPhotoUrl FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
			} 
			return currentUser;
		} set;
	}

	public String contactUsLink {
		get {
			return HRP_Constants.HR_CONTACT_US_LINK;
		} set;
	}

	public HRP_HeaderController() {}
}