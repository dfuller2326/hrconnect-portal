@isTest(SeeAllData=false)
public with sharing class HRP_TestDataFactory {

  public static List<HRP_Settings__c> buildHRPSettings(){
    return (new List<HRP_Settings__c> {
      new HRP_Settings__c( Name = 'PrimarySearch1'
        , Type__c = 'PrimarySearch'
        , Text1__c = 'Procedure__kav')
      , new HRP_Settings__c( Name = 'SecondarySearch1'
        , Type__c = 'SecondarySearch'
        , Text1__c = 'FAQ__kav')
    });
  }

  public static Procedure__kav buildProcedureArticle() {
    String rand = String.valueOf(Math.Mod(Math.Round((Math.Random() * 10000)),10000));
    Procedure__kav prodArticle = new Procedure__kav(Summary = 'Intro'+ rand
      , IsVisibleInPkb = true
      , Title = 'Procedure Title' + rand
      , UrlName = 'artcleURLForProcdure' + rand
      , Language = 'en_US'
      , Keywords__c = 'Remove people who have opted out from your contact lists.');
    return prodArticle;
  }

  public static Procedure__DataCategorySelection buildDataCategorySelectionForProcedure() {
    Procedure__DataCategorySelection procDataCategory = new Procedure__DataCategorySelection(DataCategoryName = 'My_Team', DataCategoryGroupName = 'Content_Category');
    return procDataCategory;
  }

  public static FAQ__kav buildFAQArticle() {
    String rand = String.valueOf(Math.Mod(Math.Round((Math.Random() * 10000)),10000));
    FAQ__kav faqArticle = new FAQ__kav(Summary = 'Intro' + rand
      , IsVisibleInPkb = true
      , Title = 'How To Title' + rand
      , UrlName = 'artcleURLForFAQ' + rand
      , Language = 'en_US'
      , Keywords__c = 'Remove people who have opted out from your contact lists.');
    return faqArticle;
  }

  public static FAQ__DataCategorySelection buildDataCategorySelectionForFAQ() {
    FAQ__DataCategorySelection faqDataCategory = new FAQ__DataCategorySelection(DataCategoryName = 'My_Team', DataCategoryGroupName = 'Content_Category');
    return faqDataCategory;
  }

  public static Job_Onboarding__kav buildJobOnBoardingArticle() {
    String rand = String.valueOf(Math.Mod(Math.Round((Math.Random() * 10000)),10000));
    Job_Onboarding__kav jobOnboardingArticle = new Job_Onboarding__kav(Summary = 'Intro' + rand
      , IsVisibleInPkb = true
      , Title = 'How To Title' + rand
      , UrlName = 'artcleURLForJobOnboarding' + rand
      , Language = 'en_US'
      , Day_1__c = 'Day 1'
      , Week_1__c = 'Week 1'
      , Month_1__c = 'Month 1'
      , Month_3__c = 'Month 3'
      , Overview__c = 'Year 1');
    return jobOnboardingArticle;
  }

  public static HRP_Content__c buildHRPContent(Id recordTypeId) {
    HRP_Content__c hrpContent = new HRP_Content__c(
    Start_Date__c = Datetime.now().addDays(-5)
    , End_Date__c = Datetime.now().addDays(30)
    , Countries__c = HRP_Constants.HR_DEFAULT_COUNTRY
    , User_Profile__C = HRP_Constants.HR_DEFAULT_PROFILE);
    if(String.isNotBlank(recordTypeId)) hrpContent.RecordTypeId = recordTypeId;
    System.debug('hrpContent=='+hrpContent);
    return hrpContent;
  }
}