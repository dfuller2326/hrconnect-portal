public class HRConnectCaseNavController {
	public String pagename {get;set;}
	public Boolean pos_enabled {get;set;}
	public Boolean track_enabled {get;set;}
	string userprofile = HRConnectCommon.userprofilename;
	boolean ismgr = HRConnectCommon.ismanager;

	public HRConnectCaseNavController()
	{
		pos_enabled = true;
		track_enabled = true;
		/*if (userprofile.contains('Exec') || userprofile.contains('Manager') || userprofile.contains('HR') || ismgr)
        {
        	pos_enabled = true;
		}*/
		if (userprofile.contains(BiogenConstants.PROSPECTIVE_EMPLOYEE_PROFILE) || userprofile.contains(BiogenConstants.FORMER_EMPLOYEE_PROFILE))
        {
            pos_enabled = false;
            track_enabled = false;
        }
		
	}
	public class NavigationItems
	{
		public String imagename {get;set;}
		public String coltitle {get;set;}
		public String bsclass {get;set;}
		public String loc {get;set;}

		NavigationItems(String img, String title, String bsc, String loc)
		{
			imagename = img;
			coltitle = title;
			bsclass = bsc;
			this.loc = loc;
		}
	}
	List<NavigationItems> navitems;

	public List<NavigationItems> getNavitems()
	{
		if(navitems == null) {
            navitems = new List<NavigationItems>();

            if (pagename == 'CASE')
            	navitems.add(new NavigationItems('tab.png','Contact Us','bgcasenav_active','/apex/HRConnectCase'));
            else
            	navitems.add(new NavigationItems('tab_selected.png','Contact Us','bgcasenav_inactive','/apex/HRConnectCase'));

            if (pagename == 'TRACK')
            	navitems.add(new NavigationItems('tab.png','Track your request','bgcasenav_active','/apex/HRConnectTrack'));
            else
            	navitems.add(new NavigationItems('tab_selected.png','Track your request','bgcasenav_inactive','/apex/HRConnectTrack'));

            if (pagename == 'POSITION')
	            	navitems.add(new NavigationItems('tab.png','Create a new position','bgcasenav_active','/apex/HRConnectPosition'));
	            else
	            	navitems.add(new NavigationItems('tab_selected.png','Create a new position','bgcasenav_inactive','/apex/HRConnectPosition'));
        }
        return navitems;
	}
}