// Controller class for the Force.com Typeahead component
public with sharing class HRP_SearchBarController {

  @RemoteAction
  public static list<sObject> searchRecords( String queryString, String objectName, 
    list<String> fieldNames, String fieldsToSearch, String filterClause, String orderBy, Integer recordLimit ) {
   
    List<SObject> results = HRP_SearchService.searchPrimaryArticles(queryString, UserInfo.getLanguage(), recordLimit);
    return results;
  }   

}