@isTest(SeeAllData=false)
private class HRP_BenefitsControllerTest {

  private static List<HRP_Content__c> hrpContentList;
  private static List<Procedure__kav> procedureArticles;

  private static void setupProcedureArticles(){
    procedureArticles = new List<Procedure__kav>();
    for(Integer i = 0; i < 5; i++){
      Procedure__kav article = HRP_TestDataFactory.buildProcedureArticle();
      article.title += ' ' + i;
      article.UrlName += i;
      procedureArticles.add(article);
    }
    insert procedureArticles;

    procedureArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId, ArticleNumber FROM Procedure__kav
      WHERE Language = 'en_US' AND PublishStatus = 'Draft'];

    for(Procedure__kav article : procedureArticles){
      KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
    }
  }

  private static void setupHRPContent(String recordType){
    hrpContentList = new List<HRP_Content__c>();
    Id recordTypeId = [SELECT Id From RecordType Where SObjectType = 'HRP_Content__c' AND DeveloperName = :recordType][0].Id;
    for(Integer i = 0; i < 5; i++){
      HRP_Content__c hrp = HRP_TestDataFactory.buildHRPContent(recordTypeId);
      hrpContentList.add(hrp);
    }
    insert hrpContentList;
  }
    private static testMethod void getPageData_Positive() {
    	setupprocedureArticles();
    setupHRPContent(HRP_Constants.HR_CONTENT_BENEFIT);
    for(Integer i = 0; i < hrpContentList.size(); i++){
      hrpContentList[i].Article_Numbers__c = procedureArticles[i].ArticleNumber;
    }
    update hrpContentList;
    	 Test.startTest();
         HRP_BenefitsController controller =  new HRP_BenefitsController();
          String str = HRP_BenefitsController.getPageData();
           Map<String, Object> mapObj = (Map<String,Object>) JSON.deserializeUntyped(str);
           List<Object> benefitsContent = (List<Object>) mapObj.get('benefitsContent');

        Test.stopTest();
         System.assertEquals(hrpContentList.size(), benefitsContent.size());

    }

  private static testMethod void setErrorInfo_Positive() {
     Test.startTest();
       HRP_BenefitsController.BenefitsResponse respObj = new HRP_BenefitsController.BenefitsResponse();
        respObj.error = new HRP_ErrorInfo(System.Label.Home_Page_Error_Heading, 'Exception Message');
      Test.stopTest();
      System.assertNotEquals(null,respObj.error );
  }
}