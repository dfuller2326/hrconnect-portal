@isTest(SeeAllData=false)
private class HRP_OnboardingControllerTest {
	private static List<HRP_Content__c> hrpContentList;

  private static List<Job_Onboarding__kav> jobOnboardingArticles;

	private static void setupHRPContent(String recordType){
    hrpContentList = new List<HRP_Content__c>();
    Id recordTypeId = [SELECT Id From RecordType Where SObjectType = 'HRP_Content__c' AND DeveloperName = :recordType][0].Id;
    for(Integer i = 0; i < 5; i++){
      HRP_Content__c hrp = HRP_TestDataFactory.buildHRPContent(recordTypeId);
      hrpContentList.add(hrp);
    }
    insert hrpContentList;
  }


 private static void setupJobOnBoardingArticles(){
    jobOnboardingArticles = new List<Job_Onboarding__kav>();
    for(Integer i = 0; i < 1; i++){
      Job_Onboarding__kav article = HRP_TestDataFactory.buildJobOnBoardingArticle();
      article.title += ' ' + i;
      article.UrlName += i;
      jobOnBoardingArticles.add(article);
    }
    insert jobOnBoardingArticles;

    jobOnBoardingArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId, ArticleNumber FROM Job_Onboarding__kav
      WHERE Language = 'en_US' AND PublishStatus = 'Draft'];

    for(Job_Onboarding__kav article : jobOnBoardingArticles){
      KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
    }
  }


    private static testMethod void getOnboardingContent_Positive() {
        setupJobOnBoardingArticles();
        setupHRPContent(HRP_Constants.HR_CONTENT_JOB_ONBOARDING);
        for(Integer i = 0; i < jobOnBoardingArticles.size(); i++){
      hrpContentList[i].Article_Numbers__c = jobOnBoardingArticles[i].ArticleNumber;
    }
    update hrpContentList;

        Test.startTest();
         HRP_OnboardingController controller =  new HRP_OnboardingController();
          String str = HRP_OnboardingController.getOnboardingContent();
           Map<String, Object> mapObj = (Map<String,Object>) JSON.deserializeUntyped(str);
           List<Object> onboardingContent = (List<Object>) mapObj.get('onboardingContent');

        Test.stopTest();
         System.assertEquals(hrpContentList.size(), onboardingContent.size());
    }

    private static testMethod void setErrorInfo_Positive() {
       Test.startTest();
         HRP_OnboardingController.OnboardingResponse respObj = new HRP_OnboardingController.OnboardingResponse();
          respObj.error = new HRP_ErrorInfo(System.Label.Home_Page_Error_Heading, 'Exception Message');
        Test.stopTest();
        System.assertNotEquals(null,respObj.error );
    }
}