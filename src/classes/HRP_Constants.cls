public with sharing class HRP_Constants {
  public static final String HR_DEFAULT_COUNTRY = 'Global';
  public static final String HR_COUNTRIES_NAME = 'Countries__c';
  public static final String HR_DEFAULT_PROFILE = 'All';
  public static final String HR_PROFILE_NAME = 'User_Profile__c';

  public static final String HR_CONTENT_TILE = 'Home_Page_Tile';
  public static final String HR_CONTENT_CAROUSEL = 'Home_Page_Carousel';
  public static final String HR_CONTENT_BENEFIT = 'Home_Page_Benefit';
  public static final String HR_CONTENT_JOB_ONBOARDING = 'Home_Page_Job_OnBoarding';

  public static final String ARTICLE_TYPE_PROCEDURE = 'Procedure__kav';
  public static final String ARTICLE_TYPE_JOB_ONBOARDING = 'Job_Onboarding__kav';
  public static final String ARTICLE_TYPE_FAQ = 'FAQ__kav';

  public static final String PROCEDURE_URL = '/apex/HRP_ProcedureArticle';

  // Header constants
  public static final String HR_CONTACT_US_LINK = '/apex/HRConnectCase';

  // Document Constants
  public static final String HR_DOCUMENT_URL = '/sfc/servlet.shepherd/version/download/';
}