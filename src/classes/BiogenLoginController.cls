global with sharing class BiogenLoginController {
    global String username{get;set;}
    global String password{get;set;}
    global BiogenLoginController () {}
    
    global PageReference login() {
        return Site.login(username, password, null);
    }
    global PageReference loginSSO() {
        return new PageReference('https://biogen.okta.com/app/salesforce-fedid/exkolsjsctoDoOhFH0x6/sso/saml');
    }


}