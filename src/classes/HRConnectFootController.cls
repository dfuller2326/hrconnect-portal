public class HRConnectFootController {

public class FooterLink
{
        public String loc {get;set;}
        public String name {get;set;}
        public String shortname {get;set;}
        FooterLink(String name, String loc)
        {
            this.loc = String.escapeSingleQuotes(loc);
            this.name = String.escapeSingleQuotes(name);
            
            if (name.length() > 30)
                shortname = name.substring(0,30);
            else
                shortname = name;
        }       
}
public class FooterSection
    {
        public String heading {get;set;}
        public List<FooterLink> links {get;set;}
        public String bsclass {get;set;}
        public String cssmarginclass {get;set;}
        
        FooterSection(String hdr, String bsclass, String mc)
        {
            heading = hdr;
            links = new List<FooterLink>();
            //links.add(new FooterLink(name,loc));
            this.bsclass = bsclass;
            cssmarginclass = mc;
        }       
    }
    List<FooterSection> FooterSections;
    
    /*public List<RecentlyViewed> getRecentlyViewed()
    {
        if(rv == null)
        {
            rv = new List<RecentlyViewed>();
        
            for (Recently_Viewed__c v : [select Site_URL__c,name from Recently_Viewed__c 
                where User_ID__c = :UserInfo.getUserId() order by createddate desc limit 5])
            {
                rv.add(new RecentlyViewed(v.name,v.Site_URL__c));
            }
        }
        return rv;
    }*/
    public List<FooterSection> getFooterSections()
    {
        Integer bscols = 2; //min 2, max 5
        String bsclass;
        String bsclassos = '';
        String bsclasslc = 'bgcust-nav2-lastcol';
        FooterSection fs; 
        Integer lmt = 6;
        String cssmarginclass = ''; //bgcust-nav2-m1        
        
        HRConnect_Portal_Config__c cs = HRConnect_Portal_Config__c.getValues('PORTAL_CONFIG_LIST');
        if (cs.No_of_Footer_Links__c != null && cs.No_of_Footer_Links__c > 0)
            lmt = (integer)cs.No_of_Footer_Links__c;

        if(FooterSections == null) 
        {       
            string userprofile = HRConnectCommon.userprofilename;
            string usercountry = HRConnectCommon.usercountry;
            //Gather user custom permissions
            Set<Id> PersmissionSetIds = new Set<Id>();
            for(PermissionSetAssignment p : [SELECT PermissionSetId FROM PermissionSetAssignment where AssigneeId=:userinfo.getUserId()])
              PersmissionSetIds.add(p.PermissionSetId);
    
            Set<Id> customPermissionIds = new Set<Id>();
            for(SetupEntityAccess ea: [SELECT SetupEntityId FROM SetupEntityAccess WHERE SetupEntityType =: LMSPortal_Constants.CUSTOM_PERMISSION and ParentId in:PersmissionSetIds])
                customPermissionIds.add(ea.SetupEntityId);
            //Query the custom permissions that the user has access to
            Set<String> customPermissionLabelSet = new Set<String>();
                for(CustomPermission cp: [select MasterLabel from CustomPermission where Id in :customPermissionIds])
                       customPermissionLabelSet.add(cp.MasterLabel);
            system.debug('*********** custom perm:' + customPermissionLabelSet);
            //system.debug(userprofile);
            if (userprofile.contains('System Administrator') || userprofile.contains('Manager') || userprofile.contains('HR') || userprofile.contains('Exec') || userprofile.equals('Employee') || userprofile.equals('New Hire Employee')){
                bscols++;
            }
            if (userprofile.contains('System Administrator') || userprofile.contains('Manager') || userprofile.contains('HR') || userprofile.contains('Exec')){
                bscols++;
            }           
            if (userprofile.contains('System Administrator') || userprofile.contains('Exec') || userprofile.contains('HR')){
                bscols++;
                //system.debug('Exec footer: '+bscols);
            }
                
            if (bscols == 5)
            {
                bsclass = 'col-md-2';
                bsclasslc = 'bgcust-nav2-lastcol5';
                //bsclassos = 'col-md-offset-1';
                //cssmarginclass = 'bgcust-nav2-m2';
            }
            else if (bscols == 4)
            {
                bsclass = 'col-md-3';
                bsclasslc = 'bgcust-nav2-lastcol4';
            }
            else
            {
                bsclass = 'col-md-4';
            }
        
            FooterSections = new List<FooterSection>();

            //HR Connections
            fs = new FooterSection('HR Connections','bgcust-nav2-col1 '+bsclass+' '+bsclassos,cssmarginclass);
            for (Custom_Link__c v : [select URL__c,name from Custom_Link__c 
                where Section__c = 'HR Quick Links' 
                and (Location__c includes('Global') or Location__c includes(:usercountry))
                and (Profile_Role__c includes('All') or Profile_Role__c includes(:userprofile))
                and (Custom_Permission__c = null or Custom_Permission__c in :customPermissionLabelSet) 
                order by sort_order__c NULLS LAST limit :lmt])
            {
                fs.links.add(new FooterLink(v.name,v.URL__c));
            }
            
            //fs.links.add(new FooterLink('link','#'));
            while (fs.links.size() < lmt)
                fs.links.add(new FooterLink('&nbsp;'.unescapeHtml4(),''));          
            FooterSections.add(fs);  
            
            if (bscols == 5)
            {
            //Exec Connections
                fs = new FooterSection('Executive Connections',bsclass+' bgcust-nav2-col2',cssmarginclass);
                for (Custom_Link__c v : [select URL__c,name from Custom_Link__c 
                    where Section__c = 'Executive Resources' 
                    and (Location__c includes('Global') or Location__c includes(:usercountry))
                    and (Profile_Role__c includes('All') or Profile_Role__c includes(:userprofile)) 
                    and (Custom_Permission__c = null or Custom_Permission__c in :customPermissionLabelSet) 
                    order by sort_order__c NULLS LAST limit :lmt])
                {
                    fs.links.add(new FooterLink(v.name,v.URL__c));
                }
                //fs.links.add(new FooterLink('link','#'));
                while (fs.links.size() < lmt)
                    fs.links.add(new FooterLink('&nbsp;'.unescapeHtml4(),''));              
                FooterSections.add(fs);
            }
            
            //Mgr Connections
            if (bscols >= 4)
            {
                fs = new FooterSection('Manager Connections',bsclass+' bgcust-nav2-col2',cssmarginclass);
                for (Custom_Link__c v : [select URL__c,name from Custom_Link__c 
                    where Section__c = 'Manager Resources' 
                    and (Location__c includes('Global') or Location__c includes(:usercountry))
                    and (Profile_Role__c includes('All') or Profile_Role__c includes(:userprofile)) 
                    and (Custom_Permission__c = null or Custom_Permission__c in :customPermissionLabelSet) 
                    order by sort_order__c NULLS LAST limit :lmt])
                {
                    fs.links.add(new FooterLink(v.name,v.URL__c));
                }
                //fs.links.add(new FooterLink('link','#'));
                while (fs.links.size() < lmt)
                    fs.links.add(new FooterLink('&nbsp;'.unescapeHtml4(),''));              
                FooterSections.add(fs);     
            }                

            //Corporate Connections
            fs = new FooterSection('Corporate Connections',bsclass+' bgcust-nav2-col2',cssmarginclass);
            for (Custom_Link__c v : [select URL__c,name from Custom_Link__c 
                where Section__c = 'Employee Quick Links' 
                and (Location__c includes('Global') or Location__c includes(:usercountry))
                and (Profile_Role__c includes('All') or Profile_Role__c includes(:userprofile))     
                and (Custom_Permission__c = null or Custom_Permission__c in :customPermissionLabelSet) 
                order by sort_order__c NULLS LAST limit :lmt])
            {
                fs.links.add(new FooterLink(v.name,v.URL__c));
            }
            //fs.links.add(new FooterLink('link','#'));
            while (fs.links.size() < lmt)
                fs.links.add(new FooterLink('&nbsp;'.unescapeHtml4(),''));          
            FooterSections.add(fs);            
                        
            //Recently Viewed
            if (bscols >= 3)
            {
            fs = new FooterSection('Recently Viewed',bsclass+' '+bsclasslc,cssmarginclass);
            for (Recently_Viewed__c v : [select Site_URL__c,name from Recently_Viewed__c 
                where User_ID__c = :UserInfo.getUserId() order by lastmodifieddate desc limit :lmt])
            {
                fs.links.add(new FooterLink(v.name,v.Site_URL__c));
            }
            while (fs.links.size() < lmt)
                fs.links.add(new FooterLink('&nbsp;'.unescapeHtml4(),''));
            FooterSections.add(fs); 
            }                   
        }
        return FooterSections;
    }   
}