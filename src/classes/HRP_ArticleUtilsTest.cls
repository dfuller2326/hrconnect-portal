@isTest(SeeAllData = false)
private class HRP_ArticleUtilsTest {

    private static Procedure__ka procedureArticleKA;
    private static Procedure__kav procedureArticleKAV;

    private static List<SObject> objects = new List<SObject>();
    private static String articleNumber='';

    /**
     *  This method creates a test Procedure KnowledgeArticle  and Procedure KnowledgeArticleVersion
     *   assigns to class level variables
     **/
    private static void setupProcedureArticle(boolean documents, boolean faqArticles, boolean relatedArticles){
        procedureArticleKAV = HRP_TestDataFactory.buildProcedureArticle();
        procedureArticleKAV.title += 'title';
        procedureArticleKAV.UrlName += 'urlName';
        if(documents) {
            procedureArticleKAV.document_ids__c = getDocuments();
        }
        if(faqArticles){
            procedureArticleKAV.Related_FAQ_Article_Numbers__c = getFAQArticles();
        }
        if(relatedArticles){
            procedureArticleKAV.Related_Article_Numbers__c = getRelatedArticles();
        }
        insert procedureArticleKAV;

        procedureArticleKAV = [SELECT articleNumber, KnowledgeArticleId, Related_Article_Numbers__c FROM Procedure__kav WHERE Language = 'en_US'
                               AND PublishStatus = 'Draft' and id =: procedureArticleKAV.id];



        KbManagement.PublishingService.publishArticle(procedureArticleKAV.KnowledgeArticleId, true);

        procedureArticleKA= [SELECT id FROM Procedure__ka WHERE  articleNumber=: procedureArticleKAV.articleNumber];

    }

    /**
     *  This method creates a test data for articles
     * - 3 article having same article num
     * - 1 article with diff article num
     **/
    private static void setupMultipleProcedureArticle(){

        List<String> languages = new List<String> { 'fr','es' };
        List<Id> ids = new List<Id> ();

        Procedure__kav articleKAV = HRP_TestDataFactory.buildProcedureArticle();
        articleKAV.title += 'title';
        articleKAV.UrlName += 'urlName';
        insert articleKAV;

        articleKAV = [SELECT id, masterVersionId,articleNumber, KnowledgeArticleId FROM Procedure__kav WHERE PublishStatus = 'Draft' and id =: articleKAV.id];

        articleNumber = articleKAV.articleNumber;
        objects.add(articleKAV);
        KbManagement.PublishingService.publishArticle(articleKAV.KnowledgeArticleId, true);

        for(String lang : languages){
            String id = KbManagement.PublishingService.submitForTranslation(articleKAV.KnowledgeArticleId, lang, UserInfo.getUserId(), Datetime.newInstanceGmt(2065, 12,1));
            ids.add(id);
        }

        List<SObject> draftArticles = [SELECT id,masterVersionId, articleNumber, KnowledgeArticleId FROM Procedure__kav WHERE PublishStatus = 'Draft' and id = :ids];

        objects.addAll(draftArticles);

        articleKAV = HRP_TestDataFactory.buildProcedureArticle();
        articleKAV.title += 'title';
        articleKAV.UrlName += 'urlName';

        insert articleKAV;

        articleKAV = [SELECT id, masterVersionId,articleNumber, KnowledgeArticleId FROM Procedure__kav WHERE PublishStatus = 'Draft' and id =: articleKAV.id];
        objects.add(articleKAV);
    }

    private static List<SObject> setupFAQArticles(){
      List<FAQ__kav> faqArticles = new List<FAQ__kav>();
      for(Integer i = 0; i < 3; i++){
        FAQ__kav article = HRP_TestDataFactory.buildFAQArticle();
        article.title += ' ' + i;
        article.UrlName += i;
        faqArticles.add(article);
      }
      insert faqArticles;

      faqArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
      PublishStatus, KnowledgeArticleId,ArticleType, ArticleNumber  FROM FAQ__kav WHERE Language = 'en_US'
        AND PublishStatus = 'Draft'];

      for(FAQ__kav article : faqArticles){
        KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
      }
      return faqArticles;

    }

    private static List<SObject> setupProcedureArticles(){
      List<Procedure__kav> procArticles = new List<Procedure__kav>();
      for(Integer i = 0; i < 5; i++){
        Procedure__kav article = HRP_TestDataFactory.buildProcedureArticle();
        article.title += ' ' + i;
        article.UrlName += i;
        procArticles.add(article);
      }
      insert procArticles;

      procArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
      PublishStatus, KnowledgeArticleId, ArticleType, ArticleNumber FROM Procedure__kav WHERE Language = 'en_US'
        AND PublishStatus = 'Draft'];

      for(Procedure__kav article : procArticles){
        KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
      }

      return procArticles;
    }



    private static void updateVote() {
        Vote v = new Vote();
        v.ParentId = procedureArticleKA.Id;
        v.Type = '3';
        insert v;
    }

    private static String getDocuments(){
        String documentIds= '';

        List<ContentVersion> cVersionList = new List<ContentVersion>();
        for(Integer i=0;i<2;i++){
            ContentVersion cVersion = new ContentVersion(
                Title = 'Test Content ' + String.valueOf(System.currentTimeMillis())
                , ContentUrl = 'www.google.com'
            );
            cVersionList.add(cVersion);
        }
        insert cVersionList;

        List<ContentDocument> contentDocumentList = [SELECT ID FROM ContentDocument WHERE LatestPublishedVersionId IN :cVersionList];

        return getConcatFieldValue(contentDocumentList, 'Id', ';');
    }

    private static String getConcatFieldValue(List<SObject> objectList, String fieldName, String seperator){
      List<String> strList = new List<String>();
      for(SObject sobj : objectList){
        strList.add(String.valueOf(sobj.get(fieldName)));
      }
      return String.join( strList, seperator);
    }

    private static String getFAQArticles(){
        List<FAQ__kav> faqArticleList = new List<FAQ__kav>();
        for(Integer i=0;i<2;i++){
            FAQ__kav faqArticle = HRP_TestDataFactory.buildFAQArticle();
            faqArticle.title += 'title';
            faqArticle.UrlName += 'urlName';

            faqArticleList.add(faqArticle);
        }

        insert faqArticleList;

        faqArticleList = [SELECT articleNumber, KnowledgeArticleId FROM FAQ__kav WHERE Language = 'en_US'
                          AND PublishStatus = 'Draft' ];
        for(FAQ__kav faqArticle: faqArticleList){
          KbManagement.PublishingService.publishArticle(faqArticle.KnowledgeArticleId, true);
        }
        return getConcatFieldValue(faqArticleList, 'articleNumber', ';');
    }

    private static String getRelatedArticles(){
        List<Procedure__kav> procedureArticles = new  List<Procedure__kav>();
        for(Integer i=0;i<2;i++){
          Procedure__kav  procedureArticle = HRP_TestDataFactory.buildProcedureArticle();
          procedureArticle.title += 'title';
          procedureArticle.UrlName += 'urlName';
          procedureArticles.add(procedureArticle);
        }

        insert procedureArticles;

        procedureArticles = [SELECT articleNumber, KnowledgeArticleId FROM Procedure__kav WHERE Language = 'en_US'
                                 AND PublishStatus = 'Draft' ];
        for(Procedure__kav  procedureArticle : procedureArticles){
          KbManagement.PublishingService.publishArticle(procedureArticle.KnowledgeArticleId, true);
        }

        return getConcatFieldValue(procedureArticles, 'articleNumber', ';');

    }

    private static testMethod void retrieveArticleTypeVersion_positive() {
        setupProcedureArticle(false,false,false);

        SObject obj;

        Test.startTest();
        obj = HRP_ArticleUtils.retrieveArticleTypeVersion( 'en_US', procedureArticleKA.id);
        Test.stopTest();

        System.assertEquals(procedureArticleKAV.id, obj.id);
    }

    private static testMethod void retrieveArticleTypeVersion_IDNull() {

        Boolean expectedExceptionThrown =  false;
        SObject obj;

        Test.startTest();
        try{
            obj = HRP_ArticleUtils.retrieveArticleTypeVersion( 'en_US', null);
        }catch(HRP_ArticleException e){
            expectedExceptionThrown = ( e.getMessage().contains('Knowledge Article Version Id is Required') )? true : false;
        }

        Test.stopTest();
        System.AssertEquals(expectedExceptionThrown, true);
    }

    private static testMethod void retrieveArticleTypeVersion_InvalidType() {
        setupProcedureArticle(false,false,false);
        Boolean expectedExceptionThrown =  false;
        SObject obj;

        Test.startTest();
        try{
            obj = HRP_ArticleUtils.retrieveArticleTypeVersion( 'en_US', procedureArticleKAV.id);
        }catch(HRP_ArticleException e){
            expectedExceptionThrown = ( e.getMessage().contains('Article Id is not a valid Article type') )? true : false;
        }
        Test.stopTest();

        System.AssertEquals(expectedExceptionThrown, true);
    }

    private static testMethod void retrieveArticleStats_Positive() {
        setupProcedureArticle(false, false, false);
        updateVote();

        KnowledgeArticle obj;

        Test.startTest();
        obj = (KnowledgeArticle) HRP_ArticleUtils.retrieveArticleStats(procedureArticleKA.id);
        Test.stopTest();

        List<KnowledgeArticleVoteStat> kaVoteStats = obj.VoteStats;
        List<KnowledgeArticleViewStat> kaViewStats = obj.ViewStats;
        List<Vote> kaVotes = obj.Votes;

        System.AssertNotEquals(null, obj);
        System.AssertEquals(1, kaVotes.size());
    }

    private static testMethod void retrieveRelatedContentDocuments_Positive() {
        setupProcedureArticle(true,false,false);

        List<ContentDocument> contentDocumentList;

        Test.startTest();
        contentDocumentList = HRP_ArticleUtils.retrieveRelatedContentDocuments( procedureArticleKA.id ,'Document_Ids__c' );
        Test.stopTest();

        System.AssertEquals(2, contentDocumentList.size());
    }

    private static testMethod void retrieveRelatedContentDocuments_IdNull() {

        List<ContentDocument> contentDocumentList;

        Test.startTest();
        contentDocumentList = HRP_ArticleUtils.retrieveRelatedContentDocuments( null ,'Document_Ids__c' );
        Test.stopTest();

        System.AssertEquals(0, contentDocumentList.size());
    }

    private static testMethod void retrieveRelatedContentDocuments_InvalidIdType() {
        setupProcedureArticle(true,false,false);
        Boolean expectedExceptionThrown =  false;
        List<ContentDocument> contentDocumentList;

        Test.startTest();
        try{
            contentDocumentList = HRP_ArticleUtils.retrieveRelatedContentDocuments( procedureArticleKAV.id ,'Document_Ids__c' );
        }catch(HRP_ArticleException e){
            expectedExceptionThrown = ( e.getMessage().contains('Article Id is not a valid Article type') )? true : false;
        }
        Test.stopTest();

        System.AssertEquals(expectedExceptionThrown, true);
    }

    private static testMethod void buildMapWithKey_Positive() {

        setupMultipleProcedureArticle();
        Map<String, List<SObject>> mapWithKey;

        Test.startTest();
          mapWithKey = HRP_ArticleUtils.buildMapWithKey( objects ,'ArticleNumber' );
          List<SObject> articles = mapWithKey.get(articleNumber);
        Test.stopTest();
        System.AssertEquals(3, articles.size());

    }

    private static testMethod void buildMapWithKeyAndValue_Positive() {
      List<SObject> sObjects = new  List<SObject>();
      List<SObject> faqArticles = setupFAQArticles();
      List<SObject> procedureArticles = setupProcedureArticles();
      sObjects.addAll(faqArticles);
      sObjects.addAll(procedureArticles);

      Map<String, List<String>> articleTypeMap;

      Test.startTest();
        articleTypeMap = HRP_ArticleUtils.buildMapWithKeyAndValue( sObjects ,'ArticleType', 'ArticleNumber');
        List<String> procArticlesFromRes = articleTypeMap.get(HRP_Constants.ARTICLE_TYPE_PROCEDURE);
        List<String> faqArticlesFromRes = articleTypeMap.get(HRP_Constants.ARTICLE_TYPE_FAQ);
      Test.stopTest();

      System.AssertEquals(procedureArticles.size(), procArticlesFromRes.size());
      System.AssertEquals(faqArticles.size(), faqArticlesFromRes.size());

  }

    private static testMethod void retrieveRelatedFAQArticles_Positive() {
        setupProcedureArticle(false,true,false);
        List<SObject> articleList;

        Test.startTest();
        articleList = HRP_ArticleUtils.retrieveRelatedArticles( procedureArticleKA.id ,'Related_FAQ_Article_Numbers__c' );
        Test.stopTest();

        System.AssertEquals(2, articleList.size());
    }

    private static testMethod void retrieveRelatedArticles_Positive() {
        setupProcedureArticle(false,false,true);

        List<SObject> articleList;

        Test.startTest();
        articleList = HRP_ArticleUtils.retrieveRelatedArticles( procedureArticleKA.id ,'Related_Article_Numbers__c' );
        Test.stopTest();

        System.AssertEquals(2, articleList.size());
    }

     private static testMethod void buildStringListFromAField_Positive() {
        setupProcedureArticle(false,false,true);

       List<String> articleNums;

        Test.startTest();
        articleNums = HRP_ArticleUtils.buildStringListFromAField( procedureArticleKAV ,'Related_Article_Numbers__c' );
        Test.stopTest();

        System.AssertEquals(2, articleNums.size());
    }

    private static testMethod void retrieveRelatedArticles_IdNull() {
        List<Document> contentDocumentList;

        Test.startTest();
        contentDocumentList = HRP_ArticleUtils.retrieveRelatedArticles( null ,'Document_Ids__c' );
        Test.stopTest();

        System.AssertEquals(0, contentDocumentList.size());
    }

    private static testMethod void retrieveRelatedArticles_InvalidIdType() {
        setupProcedureArticle(false,false,true);
        Boolean expectedExceptionThrown =  false;
        List<Document> contentDocumentList;

        Test.startTest();
        try{
            contentDocumentList = HRP_ArticleUtils.retrieveRelatedArticles( procedureArticleKAV.id ,'Document_Ids__c' );
        }catch(HRP_ArticleException e){
            expectedExceptionThrown = ( e.getMessage().contains('Article Id is not a valid Article type') )? true : false;
        }
        Test.stopTest();

        System.AssertEquals(expectedExceptionThrown, true);
    }

}