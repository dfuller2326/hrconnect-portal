global with sharing class HRP_RelatedArticlesController {
	public HRP_RelatedArticlesController() {
	}

	@RemoteAction
	global static String getPageData(Id kaId) {
		RelatedArticleResp respObj = new RelatedArticleResp();
		try {
			respObj.relatedArticles = HRP_ArticleUtils.retrieveRelatedArticles(kaId, 'Related_FAQ_Article_Numbers__c');
		} catch(Exception ex) {
			respObj.error = new HRP_ErrorInfo(System.Label.HRP_Related_Articles_Error_Heading, ex.getMessage());
			System.debug(LoggingLevel.ERROR, 'ERROR RETRIEVING RELATED ARTICLES PAGE DATA === ' + ex.getMessage());
		}
		return JSON.serialize(respObj);
	}

	public class RelatedArticleResp {
		public List<SObject> relatedArticles {get;set;}
		public HRP_ErrorInfo error {get;set;}

		public RelatedArticleResp(){

		}
	}
}