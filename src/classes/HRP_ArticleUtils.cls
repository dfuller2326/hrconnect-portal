public with sharing class HRP_ArticleUtils {
	public static SObject retrieveArticleTypeVersion (String lang, Id kaId) {
    if(String.isBlank(kaId)) throw new HRP_ArticleException('Knowledge Article Version Id is Required');
    String sObjectName = kaId.getSobjectType().getDescribe().getName();
    if(!sObjectName.endsWith('__ka')) throw new HRP_ArticleException('Article Id is not a valid Article type');
    sObjectName = sObjectName.replace('__ka', '__kav');
    System.debug('sObjectName===='+sObjectName);
    String soql = 'SELECT ';
    for(String fieldName : Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().keySet()) {
      soql += fieldName + ', ';
    }
    //Relationship Fields
    soql += 'LastModifiedBy.FirstName, LastModifiedBy.LastName';
    soql += ' FROM ' + sObjectName + ' WHERE Language = \'' + String.escapeSingleQuotes(lang) + '\' AND ' +  'PublishStatus = \'Online\' AND ' +'KnowledgeArticleId = \'' + String.escapeSingleQuotes(kaId) + '\' UPDATE VIEWSTAT';
    System.debug('soql==='+soql);
    List<SObject> sObjectList = (List<SObject>) Database.query(soql);
    if(sObjectList.isEmpty()) throw new HRP_ArticleException('ArticleTypeVersion is not found for the specified KnowledgeArticleVersionId');
    System.debug('ARTICLE TYPE VERSION LIST === ' + sObjectList[0]);
    return sObjectList[0];
  }

  public static SObject retrieveArticleStats(Id kaId) {
    return ([SELECT Id, (Select Id, ParentId, Type, CreatedById FROM Votes), (Select Id, Channel, ParentId, NormalizedScore FROM ViewStats), (Select Id, Channel, ParentId, NormalizedScore FROM VoteStats) FROM KnowledgeArticle WHERE Id = :kaId]);
  }

  // Build the soql query based on the articletype, user's language and article numbers and return the article records
  public static List<SObject> retrieveArticles(List<String> articleNums, String articleType){
    List<SObject> articleList = new List<SObject>();
    if(articleNums == null || articleNums.isEmpty()) return articleList;

    String soql = 'SELECT ';
    for(String fieldName : Schema.getGlobalDescribe().get(articleType).getDescribe().fields.getMap().keySet()) {
      soql += fieldName + ', ';
    }
    soql = soql.left(soql.lastIndexOf(','));
    soql += ' FROM ' + articleType + ' WHERE publishStatus=\'Online\' AND language=\'' + UserInfo.getLanguage() + '\'' + ' AND ArticleNumber IN :articleNums';

    System.debug('soql==='+soql);

    articleList = (List<SObject>) Database.query(soql);
    return articleList;
  }

  // Build the soql query based on the articletype, user's language and article numbers and return the article records
  public static List<SObject> retrieveArticles(List<String> articleNums){
    List<SObject> articleList = new List<SObject>();
    if(articleNums == null || articleNums.isEmpty()) return articleList;

    String soql = 'SELECT Id, ArticleType, ArticleNumber, KnowledgeArticleId ';
    soql += ' FROM KnowledgeArticleVersion WHERE publishStatus=\'Online\' AND language=\'' + UserInfo.getLanguage() + '\'' + ' AND ArticleNumber IN :articleNums';

    List<SObject> kaList = (List<SObject>) Database.query(soql);

    if(kaList == null || kaList.isEmpty()) return articleList;

    Map<String, List<String>> articleTypeMap = buildMapWithKeyAndValue(kaList, 'ArticleType', 'ArticleNumber');
    Map<String, SObject> articleNumToSObjMap = new Map<String, SObject>();

    for(String articleType : articleTypeMap.keySet()){
      for(SObject sObj :  retrieveArticles(articleTypeMap.get(articleType), articleType)){
        articleNumToSObjMap.put(String.valueOf(sObj.get('ArticleNumber')), sObj);
      }
    }

    for(String articleNum : articleNums){
      if(!articleNumToSObjMap.containsKey(articleNum)) continue;
      articleList.add(articleNumToSObjMap.get(articleNum));
    }
    return articleList;
  }

  public static List<SObject> retrieveRelatedArticles(Id articleId, String fieldName) {
    List<SObject> articleList = new List<SObject>();
    if(String.isBlank(articleId)) return articleList;

    String sObjectName = articleId.getSobjectType().getDescribe().getName();
    if(!sObjectName.endsWith('__ka')) throw new HRP_ArticleException('Article Id is not a valid Article type');
    sObjectName = sObjectName.replace('__ka', '__kav');

    String soql = 'SELECT Id, ArticleType, ArticleNumber, KnowledgeArticleId, ' + fieldName;
    soql += ' FROM ' + sObjectName + ' WHERE publishStatus=\'Online\' AND language=\'' + UserInfo.getLanguage() + '\'' + ' AND KnowledgeArticleId = :articleId';

    List<SObject> kavList = (List<SObject>) Database.query(soql);

    if(kavList == null || kavList.isEmpty()) return articleList;

    return retrieveArticles(buildStringListFromAField(kavList[0], fieldName));

  }

  public static List<ContentDocument> retrieveRelatedContentDocuments(Id articleId, String fieldName) {
    List<ContentDocument> contentDocumentList = new List<ContentDocument>();
    if(String.isBlank(articleId)) return contentDocumentList;

    String sObjectName = articleId.getSobjectType().getDescribe().getName();
    if(!sObjectName.endsWith('__ka')) throw new HRP_ArticleException('Article Id is not a valid Article type');
    sObjectName = sObjectName.replace('__ka', '__kav');

    String soql = 'SELECT Id, ArticleType, ArticleNumber, KnowledgeArticleId, ' + fieldName;
    soql += ' FROM ' + sObjectName + ' WHERE publishStatus=\'Online\' AND language=\'' + UserInfo.getLanguage() + '\'' + ' AND KnowledgeArticleId = :articleId';

    List<SObject> kavList = (List<SObject>) Database.query(soql);

    if(kavList == null || kavList.isEmpty()) return contentDocumentList;

    return retrieveContentDocuments(buildStringListFromAField(kavList[0], fieldName));
  }

  public static List<ContentDocument> retrieveContentDocuments(List<String> contentDocumentIds) {
    System.debug('DOCUMENT IDS :: ' + contentDocumentIds);
    List<ContentDocument> contentDocumentList = new List<ContentDocument>();
    if(contentDocumentIds == null || contentDocumentIds.isEmpty()) return contentDocumentList;

    String soql = 'SELECT Id, Title, FileType, FileExtension, LatestPublishedVersionId';
    soql += ' FROM ContentDocument WHERE Id IN :contentDocumentIds';

    contentDocumentList = (List<ContentDocument>) Database.query(soql);
    System.debug('DOCUMENT LIST :: ' + contentDocumentList);
    if(contentDocumentList == null || contentDocumentList.isEmpty()) return contentDocumentList;

    return contentDocumentList;
  }

  // Build a map from the list of SObjects and use the field name as the key to the map
  public static Map<String, List<SObject>> buildMapWithKey(List<SObject> sObjList, String fieldName){
    Map<String, List<SObject>> sObjMap = new Map<String, List<SObject>>();
    if(sObjList.isEmpty()) return sObjMap;

    for(SObject sObj : sObjList){
      String key = String.valueOf(sObj.get(fieldName));
      if(String.isBlank(key)) continue;

      if(sObjMap.containsKey(key)) {
        sObjMap.get(key).add(sObj);
      } else {
        sObjMap.put(key, new List<SObject> {sObj});
      }
    }
    return sObjMap;
  }

  // Build a map from the list of SObjects and use the field name as the key to the map
  public static Map<String, List<String>> buildMapWithKeyAndValue(List<SObject> sObjList, String keyFieldName, String valueFieldName){
    Map<String, List<String>> stringMap = new Map<String, List<String>>();
    if(sObjList.isEmpty()) return stringMap;

    for(SObject sObj : sObjList){
      String key = String.valueOf(sObj.get(keyFieldName));
      if(String.isBlank(key)) continue;

      if(stringMap.containsKey(key)) {
        stringMap.get(key).add(String.valueOf(sObj.get(valueFieldName)));
      } else {
        stringMap.put(key, new List<String> {String.valueOf(sObj.get(valueFieldName))});
      }
    }
    return stringMap;
  }

  //public static Vote rateArticle(Id kaId, String type) {
  //  if(String.isBlank(String.valueOf(kaId)) || String.isBlank(type)) return null;

  //  Vote vt = retrieveVote(kaId);
  //  if(vt != null) delete vt;

  //  vt = new Vote(ParentId = kaId, Type = type);
  //  insert vt;
  //  return vt;
  //}

  //public static Vote retrieveVote(Id kaId) {
  //  if(String.isBlank(String.valueOf(kaId))) return null;
  //  List<Vote> votes = [SELECT Id, ParentId, Type FROM VOTE WHERE ParentId =:kaId AND CreatedById =:UserInfo.getUserId()];
  //  if(votes == null || votes.isEmpty()) return null;

  //  return votes[0];
 //}
@TestVisible
  private static List<String> buildStringListFromAField(SObject sObj, String fieldName){
    List<String> articleNums = new List<String>();
    if(sObj == null) return articleNums;
    String fieldValue = String.valueOf(sObj.get(fieldName));
    if (String.isBlank(fieldValue)) return articleNums;
    for(String articleNum : fieldValue.split(';')){
      articleNum = articleNum.trim();
      articleNums.add(articleNum);
    }
    System.debug('ARTICLE NUMS :: ' + articleNums);
    return articleNums;
  }
}