@isTest(SeeAllData=false)
private class HRP_HeaderControllerTest {
	
	@isTest static void testCurrentUser() {
		Test.startTest();
        HRP_HeaderController ctrl =  new HRP_HeaderController();
        User userInContext = ctrl.currentUser;
        Test.stopTest();
        System.assertEquals(UserInfo.getUserId(), userInContext.Id);
        System.assertEquals(UserInfo.getLocale(), userInContext.LanguageLocaleKey);
	}

	@isTest static void testContactUsLink() {
		Test.startTest();
        HRP_HeaderController ctrl =  new HRP_HeaderController();
        String link = ctrl.contactUsLink;
        Test.stopTest();
        System.assertEquals(HRP_Constants.HR_CONTACT_US_LINK, link);
	}
}