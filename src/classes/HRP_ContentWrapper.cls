public with sharing class HRP_ContentWrapper {
	public List<SObject> articles {get; set;}
  public SObject content {get; set;}
  public HRP_ContentWrapper(List<SObject> articles, SObject content) {
		this.articles = articles;
    this.content = content;
	}
}