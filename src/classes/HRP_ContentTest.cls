@isTest(SeeAllData=false)
private class HRP_ContentTest {
  private static List<HRP_Content__c> hrpContentList;
  private static List<Procedure__kav> procedureArticles;
  private static List<Job_Onboarding__kav> jobOnboardingArticles;

  private static void setupHRPContent(String recordType){
    hrpContentList = new List<HRP_Content__c>();
    Id recordTypeId = [SELECT Id From RecordType Where SObjectType = 'HRP_Content__c' AND DeveloperName = :recordType][0].Id;
    for(Integer i = 0; i < 5; i++){
      HRP_Content__c hrp = HRP_TestDataFactory.buildHRPContent(recordTypeId);
      hrpContentList.add(hrp);
    }
    insert hrpContentList;
  }

  private static void setupProcedureArticles(){
    procedureArticles = new List<Procedure__kav>();
    for(Integer i = 0; i < 5; i++){
      Procedure__kav article = HRP_TestDataFactory.buildProcedureArticle();
      article.title += ' ' + i;
      article.UrlName += i;
      procedureArticles.add(article);
    }
    insert procedureArticles;

    procedureArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId, ArticleNumber FROM Procedure__kav
      WHERE Language = 'en_US' AND PublishStatus = 'Draft'];

    for(Procedure__kav article : procedureArticles){
      KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
    }
  }

  private static void setupJobOnBoardingArticles(){
    jobOnboardingArticles = new List<Job_Onboarding__kav>();
    for(Integer i = 0; i < 1; i++){
      Job_Onboarding__kav article = HRP_TestDataFactory.buildJobOnBoardingArticle();
      article.title += ' ' + i;
      article.UrlName += i;
      jobOnBoardingArticles.add(article);
    }
    insert jobOnBoardingArticles;

    jobOnBoardingArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId, ArticleNumber FROM Job_Onboarding__kav
      WHERE Language = 'en_US' AND PublishStatus = 'Draft'];

    for(Job_Onboarding__kav article : jobOnBoardingArticles){
      KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
    }
  }

  private static testMethod void retrieveHomePageTiles_Positive() {
    setupHRPContent(HRP_Constants.HR_CONTENT_TILE);

    List<HRP_ContentWrapper> hrpContentWrapper;
    Test.startTest();
      hrpContentWrapper = HRP_Content.retrieveHomePageTiles(HRP_Constants.HR_DEFAULT_COUNTRY,HRP_Constants.HR_DEFAULT_PROFILE);
    Test.stopTest();
    System.assertEquals(hrpContentList.size(), hrpContentWrapper.size());
  }

  private static testMethod void retrieveHomePageCarousel_Positive() {
    setupHRPContent(HRP_Constants.HR_CONTENT_CAROUSEL);

    List<HRP_ContentWrapper> hrpContentWrapper;
    Test.startTest();
      hrpContentWrapper = HRP_Content.retrieveHomePageCarousel(HRP_Constants.HR_DEFAULT_COUNTRY,HRP_Constants.HR_DEFAULT_PROFILE);
    Test.stopTest();
    System.assertEquals(hrpContentList.size(), hrpContentWrapper.size());
  }

  private static testMethod void retrieveHomePageBenefits_Positive() {
    setupprocedureArticles();
    setupHRPContent(HRP_Constants.HR_CONTENT_BENEFIT);
    for(Integer i = 0; i < hrpContentList.size(); i++){
      hrpContentList[i].Article_Numbers__c = procedureArticles[i].ArticleNumber;
    }
    update hrpContentList;

    List<HRP_ContentWrapper> hrpContentWrapper;
    Test.startTest();
      hrpContentWrapper = HRP_Content.retrieveHomePageBenefits(HRP_Constants.HR_DEFAULT_COUNTRY,HRP_Constants.HR_DEFAULT_PROFILE);
    Test.stopTest();
    System.assertEquals(hrpContentList.size(), hrpContentWrapper.size());
  }

  private static testMethod void retrieveHomePageJobOnBoarding_Positive() {
    setupJobOnBoardingArticles();
    setupHRPContent(HRP_Constants.HR_CONTENT_JOB_ONBOARDING);
    for(Integer i = 0; i < jobOnBoardingArticles.size(); i++){
      hrpContentList[i].Article_Numbers__c = jobOnBoardingArticles[i].ArticleNumber;
    }
    update hrpContentList;

    List<HRP_ContentWrapper> hrpContentWrapper;
    Test.startTest();
      hrpContentWrapper = HRP_Content.retrieveHomePageJobOnBoarding(HRP_Constants.HR_DEFAULT_COUNTRY,HRP_Constants.HR_DEFAULT_PROFILE);
    Test.stopTest();
    System.assertEquals(hrpContentList.size(), hrpContentWrapper.size());
  }
}