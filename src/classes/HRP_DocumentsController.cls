global with sharing class HRP_DocumentsController {
	public String documentURL {
		get {
			return HRP_Constants.HR_DOCUMENT_URL;
		}
		set;
	}
	public HRP_DocumentsController() {

	}

	@RemoteAction
	global static String getPageData(Id kaId) {
		DocumentsResp respObj = new DocumentsResp();
		try {
			respObj.importantDocs = HRP_ArticleUtils.retrieveRelatedContentDocuments(kaId, 'Document_Ids__c');
		} catch(Exception ex) {
			respObj.error = new HRP_ErrorInfo(System.Label.HRP_Documents_Error_Heading, ex.getMessage());
			System.debug(LoggingLevel.ERROR, 'ERROR RETRIEVING IMPORTANT DOCUMENTS PAGE DATA === ' + ex.getMessage());
		}
		return JSON.serialize(respObj);
	}

	public class DocumentsResp {
		public List<ContentDocument> importantDocs {get;set;}
		public HRP_ErrorInfo error {get;set;}

		public DocumentsResp(){

		}
	}
}