@isTest(SeeAllData = false)
private class HRP_RelatedServicesControllerTest {
  private static List<HRP_Content__c> hrpContentList;
  private static List<Procedure__kav> procedureArticles;

  private static void setupProcedureArticles() {
    procedureArticles = new List<Procedure__kav>();
    for(Integer i = 0; i < 5; i++){
      Procedure__kav article = HRP_TestDataFactory.buildProcedureArticle();
      article.title += ' ' + i;
      article.UrlName += i;
      procedureArticles.add(article);
    }
    insert procedureArticles;

    procedureArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId, ArticleNumber, Related_Article_Numbers__c FROM Procedure__kav
      WHERE Language = 'en_US' AND PublishStatus = 'Draft'];

    for(Integer i = 0; i < 5; i++){
      if( i == 0){
        procedureArticles[0].Related_Article_Numbers__c = procedureArticles[1].ArticleNumber + ';' + procedureArticles[2].ArticleNumber;
      }
    }
    update procedureArticles;

    procedureArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId, ArticleNumber, Related_Article_Numbers__c FROM Procedure__kav
      WHERE Language = 'en_US' AND PublishStatus = 'Draft'];

    for(Procedure__kav article : procedureArticles) {
      KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
    }
  }

  private static void setupHRPContent(String recordType){
    hrpContentList = new List<HRP_Content__c>();
    Id recordTypeId = [SELECT Id From RecordType Where SObjectType = 'HRP_Content__c' AND DeveloperName = :recordType][0].Id;
    for(Integer i = 0; i < 5; i++){
      HRP_Content__c hrp = HRP_TestDataFactory.buildHRPContent(recordTypeId);
      hrpContentList.add(hrp);
    }
    insert hrpContentList;
  }

  private static testMethod void getPageData_Positive() {
    setupProcedureArticles();

    Test.startTest();
      HRP_RelatedServicesController controller =  new HRP_RelatedServicesController();
      String str = HRP_RelatedServicesController.getPageData(procedureArticles[0].KnowledgeArticleId);
      Map<String, Object> mapObj = (Map<String,Object>) JSON.deserializeUntyped(str);
      List<Object> relatedServices = (List<Object>) mapObj.get('relatedServices');
    Test.stopTest();

    System.assertEquals(2, relatedServices.size());
  }

  private static testMethod void setErrorInfo_Positive() {
    setupHRPContent(HRP_Constants.HR_CONTENT_BENEFIT);

    Test.startTest();
      HRP_RelatedServicesController controller =  new HRP_RelatedServicesController();
      String str = HRP_RelatedServicesController.getPageData(hrpContentList[0].Id);
      Map<String, Object> mapObj = (Map<String,Object>) JSON.deserializeUntyped(str);
      Object error = (Object) mapObj.get('error');
    Test.stopTest();

    System.assert(error != null);
  }
}