public with sharing class HRP_ArticleRedirectController {
	public HRP_ArticleRedirectController() {}

  public PageReference redirectToArticle(){
    String articleNum = ApexPages.currentPage().getParameters().get('anum');
    String articleId = ApexPages.currentPage().getParameters().get('aid');

    // Check the parameters are blank and the return to Error Page / Null
    if(String.isBlank(articleNum) && String.isBlank(articleId)) return new PageReference('/apex/HRP_Home');

    String lang = UserInfo.getLanguage();
    if(String.isNotBlank(articleNum)){
      List<KnowledgeArticle> kaList= [SELECT Id FROM KnowledgeArticle WHERE ArticleNumber = :articleNum];
      if(kaList != null && !kaList.isEmpty()) articleId = kaList[0].Id;
    }
    if(String.isBlank(articleId)) return new PageReference('/apex/HRP_Home');

    // Build SOQL to Query the KnowledgeAricleVersion based on the User's Language and the params passed
    String query = 'SELECT Id, KnowledgeArticleId, ArticleType, Language '
      + 'FROM KnowledgeArticleVersion '
      + 'WHERE Language = :lang AND PublishStatus = \'Online\' ' + 'AND KnowledgeArticleId ';
    // If the Article number is passed use it to query the KnowledgeArticleVerison else use the Article Id
    query += ' = :articleId';

    system.debug('query===='+query);
    List<KnowledgeArticleVersion> results = Database.query(query);
    system.debug('results==='+results);

    return buildArticlePageURL(results);
  }

  // Build the PageRefernce based on the Article Type
  private PageReference buildArticlePageURL(List<KnowledgeArticleVersion> results){
    PageReference articlePgRef = new PageReference('/apex/HRP_Home');
    if(results.isEmpty()) return articlePgRef;

    if(results[0].ArticleType.equalsIgnoreCase(HRP_Constants.ARTICLE_TYPE_PROCEDURE)){
      articlePgRef = new PageReference(HRP_Constants.PROCEDURE_URL + '?id=' + results[0].KnowledgeArticleId);
    }
    system.debug('url===='+HRP_Constants.PROCEDURE_URL + '?id=' + results[0].KnowledgeArticleId);
    system.debug('pgRef=='+articlePgRef);
    return articlePgRef;
  }
}