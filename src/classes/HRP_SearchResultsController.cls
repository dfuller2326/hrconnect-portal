global with sharing class HRP_SearchResultsController {
  //Return Context User
  public User currentUser {
    get {
      return [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
    }
  }

  public HRP_SearchResultsController() {}

  // Remote Action method to retrieve all page data
  @RemoteAction
  global static String getPageData(String searchString) {
    SearchResultsResponse respObj;
    try {
      respObj = new SearchResultsResponse(HRP_DataCategoryUtils.dataCategoryWrapperMap
        , HRP_SearchService.searchPrimaryArticles(searchString
          , UserInfo.getLanguage(), null)
        , HRP_SearchService.searchSecondaryArticles(searchString
          , UserInfo.getLanguage())
      );
    } catch(Exception ex) {
      respObj = new SearchResultsResponse(new HRP_ErrorInfo(System.Label.HRP_Search_Results_Page_Error, ex.getMessage()));
    }
    System.debug('RESPONSE :: ' + JSON.serialize(respObj));

    return JSON.serialize(respObj);
  }


  // JSON Response Object Wrapper Class
  public class SearchResultsResponse {
    public HRP_ErrorInfo error {get;set;}
    public Map<String, List<HRP_DataCategoryUtils.DataCategoryWrapper>> dataCatMap {get;set;}
    public List<SObject> primaryResults {get;set;}
    public List<SObject> secondaryResults {get;set;}

    public SearchResultsResponse(Map<String, List<HRP_DataCategoryUtils.DataCategoryWrapper>> dataCatMap, List<SObject> primaryResults, List<SObject> secondaryResults) {
      this.dataCatMap = dataCatMap;
      this.primaryResults = primaryResults;
      this.secondaryResults = secondaryResults;
    }
    public SearchResultsResponse(HRP_ErrorInfo error){
      this.error = error;
    }
   }
}