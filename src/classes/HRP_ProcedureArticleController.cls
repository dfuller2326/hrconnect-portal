global with sharing class HRP_ProcedureArticleController {

  public HRP_ProcedureArticleController(ApexPages.StandardController stdCtrl) {

  }

  @RemoteAction
  global static String getPageData(Id kaId) {
      ArticleDetailResp respObj = new ArticleDetailResp();
      try {
            String lang = String.valueOf(UserInfo.getLanguage());
            respObj.article = HRP_ArticleUtils.retrieveArticleTypeVersion(lang, kaId);
            respObj.articleStats = HRP_ArticleUtils.retrieveArticleStats(kaId);
            // Call sidebar methods to determine if content is empty or not
            respObj.importantDocs = HRP_ArticleUtils.retrieveRelatedContentDocuments(kaId, 'Document_Ids__c');
            respObj.relatedArticles = HRP_ArticleUtils.retrieveRelatedArticles(kaId, 'Related_Article_Numbers__c');
            respObj.relatedQA = HRP_ArticleUtils.retrieveRelatedArticles(kaId, 'Related_FAQ_Article_Numbers__c');
          } catch(Exception ex) {
            respObj.error = new HRP_ErrorInfo(System.Label.HRP_Article_Detail_Page_Error, ex.getMessage());
            System.debug(LoggingLevel.ERROR, 'ERROR RETRIEVING ARTICLE DETAIL PAGE DATA === ' + ex.getMessage());
          }
    return JSON.serialize(respObj);
  }

  public class ArticleDetailResp {
      public SObject article {get;set;}
      public SObject articleStats {get;set;}
      List<ContentDocument> importantDocs;
      List<SObject> relatedArticles;
      List<SObject> relatedQA;
      public HRP_ErrorInfo error {get;set;}
      public Boolean hasSidebarContent {get; set;}

      public ArticleDetailResp() {
        this.hasSidebarContent = false;
      }
  }
}