public with sharing class HRP_ErrorInfo {
	// error code
	public String errCode {get; set;}
    // error message
    public String errMsg {get; set;}
    
   
    // constructor
    public HRP_ErrorInfo(String ec, String em){
        errCode = (String.isNotBlank(ec)) ? ec : '';
        errMsg = (String.isNotBlank(em)) ?  + em + ' ' + '(' + System.Label.HRP_Contact_System_Admin_Error + ')' : '';
    }
}