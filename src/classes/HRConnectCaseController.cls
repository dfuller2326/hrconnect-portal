public class HRConnectCaseController {

    private ApexPages.StandardController controller;

    public transient Blob attbody1 {get; set;}
    public transient Blob attbody2 {get; set;}
    public transient Blob attbody3 {get; set;}

    public string filename1 {get;set;}
    public string filename2 {get;set;}
    public string filename3 {get;set;}

    public Boolean OnBehalfOf_Enabled{
        get{
            if (userprofile.contains(BiogenConstants.PROSPECTIVE_EMPLOYEE_PROFILE) || userprofile.contains(BiogenConstants.FORMER_EMPLOYEE_PROFILE))
            {
                return false;
            }
            return true;
        }
        set;
    }
    
    public Boolean provideEmail_Enabled{
        get{
            if (userprofile.contains(BiogenConstants.PROSPECTIVE_EMPLOYEE_PROFILE) || userprofile.contains(BiogenConstants.FORMER_EMPLOYEE_PROFILE))
            {
                return true;
            }
            return false;
        }
        set;
    }

    public string OnBehalfOfId {get; set;}
    public string OnBehalfOf {get; set;}
    public boolean OnBehalfOfError {get; set;}
    public boolean emailError {get; set;}
    
    public List<KnowledgeArticleVersion> articleList {get;set;}
    public Integer offset {get;set;}
    
    public ApexPages.StandardSetController stdCon;
    
    public Id caseId {get; set;}
    
    public Attachment attachment {
        get {
          if (attachment == null)
            attachment = new Attachment();
          return attachment;
        }
        set;
    }
    public Attachment attachment2 {
        get {
          if (attachment2 == null)
            attachment2 = new Attachment();
          return attachment2;
        }
        set;
    }
    public Attachment attachment3 {
        get {
          if (attachment3 == null)
            attachment3 = new Attachment();
          return attachment3;
        }
        set;
    }
    static String userlang = HRConnectCommon.userlang;
    static string userprofile = HRConnectCommon.userprofilename;
    public String SortSearch {get; set;}
    public Integer page_size {get; set;}
    public Integer currentPage {get; set;}
    public String searchString {get; set;}
    //public String pagetext {get; set;}
    public String pagetext {
        get{
            if(currentPage!=null && maxSize!=null && maxSize !=0 && page_size!=null && page_size!=0 )
            {
                //return '( '+currentPage+' Of '+((decimal)maxSize).divide((decimal)page_size,0,system.RoundingMode.CEILING)+' )';
                Integer startNumber = offset + 1;
                Integer endNumber = offset + page_size;
                if(endNumber > maxSize){
                    endNumber = maxSize;    
                }
                return 'Showing ' + startNumber + ' - ' + endNumber + ' of ' +  maxSize + ' Articles';
            }
            else
                return 'Not Available';  
        }
        set;
    }
    public Integer maxSize {get; set;}
    public Boolean confirmCheckSuggestedArticles {get; set;}
    
    // Returns whether we need to see previous button or not
    public boolean getPrevRequired() {
        return offset > 0;
    }
    // Returns whether we need to see next button or not
    public boolean getNextRequired() {
        return offset + page_size < maxSize;
    }
    //Returns current page number
    public Decimal getCurrentPageNumber() {
        return this.currentPage;
    }
    //action for next click
    public PageReference next() {
        /*if(maxSize > this.currentPage * page_size) {
            this.currentPage = this.currentPage + 1;
        }*/
        offset = offset + page_size;
        return null;
    }
    //action for previous click
    public PageReference previous() {
        /*if(this.currentPage > 1)
            this.currentPage = this.currentPage - 1;*/
        offset = Math.max(0, offset - page_size);
        return null;
    }
    
    public HRConnectCaseController(ApexPages.StandardController stdController) {
        OnBehalfOfError = false;
        emailError = false;
        this.controller = stdController;
        successflag = 'N';
        Case hrcase = (Case)controller.getRecord();
        /*hrcase.Subject = 'Subject';
        hrcase.description = 'Enter your question here...';
        hrcase.SuppliedEmail = 'Provide your Email Address';*/
        hrcase.Subject = '';
        hrcase.description = '';
        hrcase.SuppliedEmail = '';
        
        SortSearch = 'mostViewed';
        page_size = 5;
        currentPage = 1;
        maxSize = 0;
        confirmCheckSuggestedArticles = false;
        offset = 0;
    }
    
    public PageReference assignSubjectToSearchString(){
        /*String subject = (String) Apexpages.currentPage().getParameters().get('subject');
        System.debug('AssignSubject: '+subject);
        searchString = subject;
        return searchString;*/
        currentPage = 1;
        confirmCheckSuggestedArticles = false;
        maxSize = 0;
        if(searchString != null && searchString.length() < 2){  
            if(articleList!=null)
                articleList.clear();     
            return null;
        }
        //String searchquery = 'FIND \'' + String.escapeSingleQuotes(searchString) + '\' IN ALL FIELDS RETURNING  KnowledgeArticleVersion(Id, title, UrlName, LastPublishedDate,LastModifiedById where PublishStatus =\'online\' and Language = \''+userlang+'\') LIMIT 100';
        //List<List<SObject>>searchList = search.query(searchquery);
        List<sObject> searchList = HRP_SearchService.searchSecondaryArticles(searchString, UserInfo.getLanguage());
        articleList = (List<KnowledgeArticleVersion>)searchList;
        maxSize = searchList.size() ;
        offset = 0;
        return null;
        
    }
    
    public PageReference doConfirmCheckSuggestedArticlesAndSave(){
        return doSave();
    }
    
    public PageReference doSave() {
        /*if(maxSize > 0 && !confirmCheckSuggestedArticles){
                System.debug('&&& Not Confirmed: ' + maxSize + ' : ' + confirmCheckSuggestedArticles);
                confirmCheckSuggestedArticles = true;
                return null;
        }*/
        
        System.debug('&&& Confirmed: ' + maxSize + ' : ' + confirmCheckSuggestedArticles);
        system.debug('&&& On Behalf Of: ' + OnBehalfOf);
        system.debug('&&& On Behalf Of ID: ' + OnBehalfOfID);
        if(String.isBlank(OnBehalfOfId) && !String.isEmpty(OnBehalfOf) && OnBehalfOf != 'On Behalf Of' ) {
            OnBehalfOfError = true;
            return null;
        }
        else {
            OnBehalfOfError = false;
        }

        Savepoint sp = Database.setSavepoint();
        Case hrcase = (Case)controller.getRecord();

        try {
        Id userid = UserInfo.getUserId();
        caseId = (Id)(ApexPages.currentPage().getParameters().get('caseId'));
        if(caseId != null){
            hrcase.ParentId = caseId;
        }
        System.debug('Test: '+userprofile);
        /*If user profile is Benefits/Former employee, there is no 'On Behalf of User' and Contact name is empty */
        if (userprofile.contains(BiogenConstants.PROSPECTIVE_EMPLOYEE_PROFILE) || userprofile.contains(BiogenConstants.FORMER_EMPLOYEE_PROFILE)) {
            hrcase.ContactId = null;
        }
        else {
            //case contact
            if (OnBehalfOfId != '' && OnBehalfOfId != null)
            {
                hrcase.On_Behalf_Of__c = OnBehalfOfId;
                userid = OnBehalfOfId;
            }
            User u = [select Employee_ID__c from user where id=:userid];
            List<Contact> c = [select id from contact where Employee_ID__c = :u.Employee_ID__c];
            if (c.size() > 0)
                hrcase.ContactId = c[0].id;
        }
        
        //record type
        RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Case' and DeveloperName = 'General'];
        hrcase.RecordTypeId = RecType.id;

        //case origin
        hrcase.origin = 'Web';
        hrcase.Portal_Case__c = true;

        // assignment rule
        //Creating the DMLOptions for "Assign using active assignment rules" checkbox
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.useDefaultRule=true;
        hrcase.setOptions(dmlOpts);


        try{
            insert hrcase;
            
            
        }
        catch (Exception e){
            if(e.getMessage().contains('INVALID_EMAIL_ADDRESS')){
                emailError = true;
            } 
            return null;
        }

        //system.assertNotEquals(null, hrcase.Id); // BOOM!
        if (hrcase.Id == null)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Case creation failed, please retry.'));
            return null;
        }

        try {
        if (filename1 != null && filename1 != '')
        {
        
            System.Debug( 'CASE DETAILS *********** '+ hrcase );
            System.Debug( 'CASE OWNER NAME *********** '+ hrcase.Owner.Name );
            
            
            attachment.body = attbody1;
            attachment.name = filename1;
            attachment.OwnerId = UserInfo.getUserId();
            attachment.ParentId = hrcase.Id; // the record the file is attached to
            //attachment.IsPrivate = true;
            insert attachment;
        }
        if (filename2 != null && filename2 != '')
        {
            attachment2.body = attbody2;
            attachment2.name = filename2;
            attachment2.OwnerId = UserInfo.getUserId();
            attachment2.ParentId = hrcase.Id; // the record the file is attached to
            //attachment.IsPrivate = true;
            insert attachment2;
        }
        if (filename3 != null && filename3 != '')
        {
            attachment3.body = attbody3;
            attachment3.name = filename3;
            attachment3.OwnerId = UserInfo.getUserId();
            attachment3.ParentId = hrcase.Id; // the record the file is attached to
            //attachment.IsPrivate = true;
            insert attachment3;
        }
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The following exception has occurred: ' + e.getMessage()));
            //return null;
        }
        finally
        {
            //clear viewstate
            attachment.body = null;
            attachment2.body = null;
            attachment3.body = null;
            filename1 = null;
            attbody1 = null;
            filename2 = null;
            attbody2 = null;
            filename3 = null;
            attbody3 = null;
        }

        if (ApexPages.hasMessages(ApexPages.Severity.ERROR) || ApexPages.hasMessages(ApexPages.Severity.FATAL))
        {
            Database.rollback(sp);
            hrcase.id = null;}
        else
            successflag = 'Y';
        } // outer try

        catch(Exception e)
        {
            Database.rollback(sp);
            hrcase.id = null;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The following exception has occurred: ' + e.getMessage()));
        }
        return null;
    }
    public List<SelectOption> getOnBehalfOfUsers() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('','On Behalf Of'));
            for (User  q : [SELECT name FROM User where (profile.name like '%Exec%' or Grade__c >= 20) order by name limit 999]){
                options.add(new SelectOption(q.id,q.name));
            }
            return options;
        }

    public String successflag {get;set;}
}