global with sharing class HRP_RelatedServicesController {
	public HRP_RelatedServicesController() {

	}

	@RemoteAction
	global static String getPageData(Id kaId) {
		RelatedServicesResp respObj = new RelatedServicesResp();
		try {
			respObj.relatedServices = HRP_ArticleUtils.retrieveRelatedArticles(kaId, 'Related_Article_Numbers__c');
		} catch(Exception ex) {
			respObj.error = new HRP_ErrorInfo(System.Label.HRP_Related_Services_Error_Heading, ex.getMessage());
			System.debug(LoggingLevel.ERROR, 'ERROR RETRIEVING RELATED SERVICES PAGE DATA === ' + ex.getMessage());
		}
		return JSON.serialize(respObj);
	}

	public class RelatedServicesResp {
		public List<SObject> relatedServices {get;set;}
		public HRP_ErrorInfo error {get;set;}

		public RelatedServicesResp() {

		}
	}
}