@isTest(SeeAllData = false)
private class HRConnectAnswerControllerTest {
    private static List<FAQ__kav> faqArticles;
    private static List<Procedure__kav> procArticles;
    private static List<HRP_Settings__c> hrpSettings;

    private static void setupFAQArticles(){
      faqArticles = new List<FAQ__kav>();
      for(Integer i = 0; i < 3; i++){
        FAQ__kav article = HRP_TestDataFactory.buildFAQArticle();
        article.title += ' ' + i;
        article.UrlName += i;
        faqArticles.add(article);
      }
      insert faqArticles;

      faqArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
      PublishStatus, KnowledgeArticleId,ArticleType, ArticleNumber  FROM FAQ__kav WHERE Language = 'en_US'
        AND PublishStatus = 'Draft'];

      for(FAQ__kav article : faqArticles){
        KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
      }
    }

    private static void setupProcedureArticles(){
      procArticles = new List<Procedure__kav>();
      for(Integer i = 0; i < 3; i++){
        Procedure__kav article = HRP_TestDataFactory.buildProcedureArticle();
        article.title += ' ' + i;
        article.UrlName += i;
        procArticles.add(article);
      }
      insert procArticles;

      procArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
      PublishStatus, KnowledgeArticleId, ArticleType, ArticleNumber FROM Procedure__kav WHERE Language = 'en_US'
        AND PublishStatus = 'Draft'];

      for(Procedure__kav article : procArticles){
        KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
      }
    }

    private static void setupHRPSettings(){
      hrpSettings = new List<HRP_Settings__c>();

      for(Integer i = 0; i < faqArticles.size(); i++) {
        hrpSettings.add(new HRP_Settings__c(
            Name = faqArticles[i].KnowledgeArticleId
          , Type__c = 'ArticleRedirect'
          , Text1__c = procArticles[i].ArticleNumber
        ));
      }
      insert hrpSettings;
    }

    private static void setupHRConnectConfig(){
      HRConnect_Portal_Config__c cs = new HRConnect_Portal_Config__c(
        Name = 'PORTAL_CONFIG_LIST'
      , New_Hire_Welcome_Link__c = 'www.google.com'
      , Answers_Page_Size__c = 10
      , No_of_Footer_Links__c = 5);

      insert cs;
    }

    private static testMethod void Test_redirectTo_Positive() {
        setupFAQArticles();
        setupProcedureArticles();
        setupHRPSettings();
        setupHRConnectConfig();

        Test.startTest();
          PageReference hrConnectAnswersPage = Page.HRConnectAnswers;
          hrConnectAnswersPage.getParameters().put('id', faqArticles[0].KnowledgeArticleId);
          Test.setCurrentPage(hrConnectAnswersPage);
          HRConnectAnswerController hrConnectAnswerCtrl = new HRConnectAnswerController();
          PageReference pgRef = hrConnectAnswerCtrl.redirectTo();
        Test.stopTest();

        System.assertEquals('/apex/HRP_ArticleRedirect?anum='+hrpSettings[0].Text1__c, pgRef.getUrl());
    }

    private static testMethod void Test_redirectTo_Negative() {
        setupFAQArticles();
        setupProcedureArticles();
        setupHRConnectConfig();

        Test.startTest();
          PageReference hrConnectAnswersPage = Page.HRConnectAnswers;
          hrConnectAnswersPage.getParameters().put('id', faqArticles[0].KnowledgeArticleId);
          Test.setCurrentPage(hrConnectAnswersPage);
          HRConnectAnswerController hrConnectAnswerCtrl = new HRConnectAnswerController();
          PageReference pgRef = hrConnectAnswerCtrl.redirectTo();
        Test.stopTest();

        System.assertEquals(null, pgRef);
    }
}