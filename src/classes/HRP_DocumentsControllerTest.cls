@isTest(SeeAllData=false)
private class HRP_DocumentsControllerTest {
  private static List<HRP_Content__c> hrpContentList;
  private static List<Procedure__kav> procedureArticles;
  private static List<ContentDocument> contentDocumentList;

  private static void setupProcedureArticles() {
    procedureArticles = new List<Procedure__kav>();
    for(Integer i = 0; i < 5; i++){
      Procedure__kav article = HRP_TestDataFactory.buildProcedureArticle();
      article.title += ' ' + i;
      article.UrlName += i;
      procedureArticles.add(article);
    }
    insert procedureArticles;

    procedureArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId, ArticleNumber, Related_Article_Numbers__c,Related_FAQ_Article_Numbers__c FROM Procedure__kav
      WHERE Language = 'en_US' AND PublishStatus = 'Draft'];

    for(Integer i = 0; i < 5; i++){
      if( i == 0){
        procedureArticles[0].Document_Ids__c = contentDocumentList[0].Id + ';' + contentDocumentList[1].Id;
      }
    }
    update procedureArticles;

    procedureArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId, ArticleNumber, Related_Article_Numbers__c,Related_FAQ_Article_Numbers__c FROM Procedure__kav
      WHERE Language = 'en_US' AND PublishStatus = 'Draft'];

    for(Procedure__kav article : procedureArticles) {
      KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
    }
  }

  private static void setupHRPContent(String recordType){
    hrpContentList = new List<HRP_Content__c>();
    Id recordTypeId = [SELECT Id From RecordType Where SObjectType = 'HRP_Content__c' AND DeveloperName = :recordType][0].Id;
    for(Integer i = 0; i < 5; i++){
      HRP_Content__c hrp = HRP_TestDataFactory.buildHRPContent(recordTypeId);
      hrpContentList.add(hrp);
    }
    insert hrpContentList;
  }

  private static void setupDocuments(){

    List<ContentVersion> cVersionList = new List<ContentVersion>();
    for(Integer i=0;i<2;i++){
        ContentVersion cVersion = new ContentVersion(
            Title = 'Test Content ' + String.valueOf(System.currentTimeMillis())
            , ContentUrl = 'www.google.com'
        );
        cVersionList.add(cVersion);
    }
    insert cVersionList;

    contentDocumentList = [SELECT ID FROM ContentDocument WHERE LatestPublishedVersionId IN :cVersionList];
  }

  private static testMethod void getPageData_Positive() {
    setupDocuments();
    setupProcedureArticles();

    Test.startTest();
      HRP_DocumentsController controller =  new HRP_DocumentsController();
      String str = HRP_DocumentsController.getPageData(procedureArticles[0].KnowledgeArticleId);
      Map<String, Object> mapObj = (Map<String,Object>) JSON.deserializeUntyped(str);
      List<Object> importantDocs = (List<Object>) mapObj.get('importantDocs');
    Test.stopTest();

    System.assertEquals(2, importantDocs.size());
    System.assertEquals(HRP_Constants.HR_DOCUMENT_URL, controller.documentURL);
  }

  private static testMethod void setErrorInfo_Positive() {
    setupHRPContent(HRP_Constants.HR_CONTENT_BENEFIT);
    setupDocuments();
    setupProcedureArticles();

    Test.startTest();
      HRP_DocumentsController controller =  new HRP_DocumentsController();
      String str = HRP_DocumentsController.getPageData(hrpContentList[0].Id);
      Map<String, Object> mapObj = (Map<String,Object>) JSON.deserializeUntyped(str);
      Object error = (Object) mapObj.get('error');
    Test.stopTest();

    System.assert(error != null);
  }
}