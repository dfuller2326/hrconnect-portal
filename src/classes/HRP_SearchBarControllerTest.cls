@isTest(SeeAllData=false)
private class HRP_SearchBarControllerTest {
	
  private static List<HRP_Settings__c> hrpSettings = new List<HRP_Settings__c>();
  private static List<Procedure__kav> procArticles = new List<Procedure__kav>();
  private static List<Id> procArticleIds = new List<Id>();
  
  private static void setupHRPSettings(){
    hrpSettings = HRP_TestDataFactory.buildHRPSettings();
    insert hrpSettings;
  }
  
   private static void setupProcedureArticles(){
    for(Integer i = 0; i < 5; i++){
      Procedure__kav article = HRP_TestDataFactory.buildProcedureArticle();
      article.title += ' ' + i;
      article.UrlName += i;
      procArticles.add(article);
    }
    insert procArticles;

    procArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId FROM Procedure__kav WHERE Language = 'en_US'
      AND PublishStatus = 'Draft'];

    for(Procedure__kav article : procArticles){
      KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
    }

    for(Integer i = 0; i < 5; i++){
      procArticleIds.add(procArticles[i].Id);
    }
   
  }
  
    
    
   private static testMethod void searchRecords_Positive() {
    	
    	setupHRPSettings();
        setupProcedureArticles();

        List<SObject> resultSet;
        Test.startTest();
         Test.setFixedSearchResults(procArticleIds);
         resultSet = HRP_SearchBarController.searchRecords('Procedure',null,null,null,null ,null, 4);
         
        Test.stopTest();
        
        System.assertEquals(4, resultSet.size());
        
    }
}