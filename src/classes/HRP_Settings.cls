public with sharing class HRP_Settings {
  public static final Map<String, HRP_Settings__c> HRP_SETTINGS = HRP_Settings__c.getAll();

	// Retrieve a map based on the fieldName of the Custom Setting:HRP_Settings__c
  public static Map<String, List<HRP_Settings__c>> getSettingsByType(String type, String name) {
    Map<String, List<HRP_Settings__c>> valueByType =
      new Map<String, List<HRP_Settings__c>>();
    if(HRP_SETTINGS == null || HRP_SETTINGS.isEmpty()) return valueByType;

    String fieldName = String.isNotBlank(name) ? name : 'name';
    for(HRP_Settings__c hrcs : HRP_SETTINGS.values()){
      if(hrcs.Type__c.equalsIgnoreCase(type)){
        String key = String.valueOf(hrcs.get(fieldName));
        if(valueByType.containsKey(key)) {
          valueByType.get(key).add(hrcs);
        } else {
          valueByType.put(key, new List<HRP_Settings__c> {hrcs});
        }
      }
    }
    return valueByType;
  }
}