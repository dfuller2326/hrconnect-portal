@isTest(SeeAllData = false)
private class HRP_RelatedArticlesControllerTest {
  private static List<HRP_Content__c> hrpContentList;
  private static List<Procedure__kav> procedureArticles;
  private static List<FAQ__kav> faqArticles;
  private static void setupProcedureArticles() {
    procedureArticles = new List<Procedure__kav>();
    for(Integer i = 0; i < 5; i++){
      Procedure__kav article = HRP_TestDataFactory.buildProcedureArticle();
      article.title += ' ' + i;
      article.UrlName += i;
      procedureArticles.add(article);
    }
    insert procedureArticles;

    procedureArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId, ArticleNumber, Related_FAQ_Article_Numbers__c FROM Procedure__kav
      WHERE Language = 'en_US' AND PublishStatus = 'Draft'];

    for(Integer i = 0; i < 5; i++){
      if( i == 0){
        procedureArticles[0].Related_FAQ_Article_Numbers__c = faqArticles[1].ArticleNumber + ';' + faqArticles[2].ArticleNumber;
      }
    }
    update procedureArticles;

    procedureArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId, ArticleNumber, Related_FAQ_Article_Numbers__c FROM Procedure__kav
      WHERE Language = 'en_US' AND PublishStatus = 'Draft'];

    for(Procedure__kav article : procedureArticles) {
      KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
    }
  }

  private static void setupFAQArticles(){
    faqArticles = new List<FAQ__kav>();
    for(Integer i = 0; i < 5; i++){
      FAQ__kav article = HRP_TestDataFactory.buildFAQArticle();
      article.title += ' ' + i;
      article.UrlName += i;
      faqArticles.add(article);
    }
    insert faqArticles;

    faqArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId, ArticleNumber FROM FAQ__kav WHERE Language = 'en_US' AND PublishStatus = 'Draft'];

    for(FAQ__kav article : faqArticles){
      KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
    }
  }

  private static void setupHRPContent(String recordType){
    hrpContentList = new List<HRP_Content__c>();
    Id recordTypeId = [SELECT Id From RecordType Where SObjectType = 'HRP_Content__c' AND DeveloperName = :recordType][0].Id;
    for(Integer i = 0; i < 5; i++){
      HRP_Content__c hrp = HRP_TestDataFactory.buildHRPContent(recordTypeId);
      hrpContentList.add(hrp);
    }
    insert hrpContentList;
  }

  private static testMethod void getPageData_Positive() {
    setupFAQArticles();
    setupProcedureArticles();

    Test.startTest();
      HRP_RelatedArticlesController controller =  new HRP_RelatedArticlesController();
      String str = HRP_RelatedArticlesController.getPageData(procedureArticles[0].KnowledgeArticleId);
      Map<String, Object> mapObj = (Map<String,Object>) JSON.deserializeUntyped(str);
      List<Object> relatedArticles = (List<Object>) mapObj.get('relatedArticles');
    Test.stopTest();

    System.assertEquals(2, relatedArticles.size());
  }

  private static testMethod void setErrorInfo_Positive() {
    setupHRPContent(HRP_Constants.HR_CONTENT_BENEFIT);

    Test.startTest();
      HRP_RelatedArticlesController controller =  new HRP_RelatedArticlesController();
      String str = HRP_RelatedArticlesController.getPageData(hrpContentList[0].Id);
      Map<String, Object> mapObj = (Map<String,Object>) JSON.deserializeUntyped(str);
      Object error = (Object) mapObj.get('error');
    Test.stopTest();

    System.assert(error != null);
  }
}